﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundName
{
    NotAllowedToBranch,
    NotAllowedToCut,
    RootCatchesFire,
    RootStartsGrowing,
    RootReadyToGrow,
    RootStartsRespawning,
    RootDies,
    ChooseNewAbility,
    BranchImpactsBranch,
    BranchImpactsFirePool,
    BranchImpactsResourcePool,
    BranchImpactedByBranch,
    NewBranch,
    UISelectSound,
    UISubmitSound,
}

public class SoundManager : MonoBehaviour
{
    private static SoundManager _instance;
    public static SoundManager Instance
    {
        get
        {
            return _instance;
        }
    }

    [SerializeField]
    private AudioSource source;

    [SerializeField]
    private AudioClip[] sounds;

    private void Awake()
    {
        _instance = this;
    }

    public void PlaySound(SoundName name)
    {
        if (sounds[(int)name] != null)
        {
            source.PlayOneShot(sounds[(int)name]);
        }
    }
}
