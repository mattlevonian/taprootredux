﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonSounds : MonoBehaviour
{
    private void Start()
    {
        EventTrigger.Entry selectedEvent = new EventTrigger.Entry();
        selectedEvent.eventID = EventTriggerType.Select;
        selectedEvent.callback.AddListener(SelectedSound);
        GetComponent<EventTrigger>().triggers.Add(selectedEvent);

        EventTrigger.Entry submitEvent = new EventTrigger.Entry();
        submitEvent.eventID = EventTriggerType.Submit;
        submitEvent.callback.AddListener(SubmitSound);
        GetComponent<EventTrigger>().triggers.Add(submitEvent);
    }

    public void SelectedSound(BaseEventData data)
    {
        SoundManager.Instance.PlaySound(SoundName.UISelectSound);
    }
    public void SubmitSound(BaseEventData data)
    {
        SoundManager.Instance.PlaySound(SoundName.UISubmitSound);
    }
}
