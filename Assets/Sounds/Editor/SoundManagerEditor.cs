﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SoundManager))]
[CanEditMultipleObjects]
public class SoundManagerEditor : Editor
{
    SerializedProperty soundsProp;

    void OnEnable()
    {
        // Setup the SerializedProperties.
        soundsProp = serializedObject.FindProperty("sounds");
    }

    public override void OnInspectorGUI()
    {
        // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
        serializedObject.Update();

        if (soundsProp.arraySize != EnumUtil.GetValues<SoundName>().Count())
        {
            soundsProp.arraySize = EnumUtil.GetValues<SoundName>().Count();
        }

        // Show the custom GUI controls.
        foreach(SoundName s in EnumUtil.GetValues<SoundName>())
        {
            SerializedProperty element = soundsProp.GetArrayElementAtIndex((int)s);
            EditorGUILayout.PropertyField(element, new GUIContent(Enum.GetName(typeof(SoundName), s)));
        }

        // Apply changes to the serializedProperty - always do this in the end of OnInspectorGUI.
        serializedObject.ApplyModifiedProperties();
    }
}

public static class EnumUtil
{
    public static IEnumerable<T> GetValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>();
    }
}