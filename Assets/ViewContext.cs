﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class represents all the information and procedure to show the world to the player.
/// It handles camera movement, split-screening, screen-to-world transformation, and other things for a single local player.
/// </summary>
public class ViewContext : MonoBehaviour
{
    private Player myPlayer;

    [SerializeField]
    private Camera myCamera;

    internal Camera Camera
    {
        get
        {
            return myCamera;
        }
    }

    private EZCameraShake.CameraShaker shaker;

    [SerializeField]
    private float collisionShakeMagnitude;
    [SerializeField]
    private float impactedShakeMagnitude;
    [SerializeField]
    private float choppedShakeMagnitude;
    [SerializeField]
    private float fireShakeMagnitude;

    private void ShakeCamera(float magnitude)
    {
        // TODO better camera shake
        shaker.ShakeOnce(magnitude, 10, 0.25f, 0.25f);
    }

    internal void CatchFireVisuals(Branch branch, bool loop = false)
    {
        // TODO check whether or not the branch is in view / focused for all visuals

        ShakeCamera(fireShakeMagnitude);
        ui.FlashFireOverlay(loop);
    }

    internal void ChoppedVisuals()
    {
        ShakeCamera(choppedShakeMagnitude);
    }

    internal void ImpactedVisuals(Branch branch)
    {
        ShakeCamera(impactedShakeMagnitude);
    }

    internal void CollisionVisuals(Branch branch)
    {
        ShakeCamera(collisionShakeMagnitude);
    }

    internal void ResourcesVisuals(Branch branch)
    {
        ui.FlashResourcesOverlay();
    }

    internal void RootReadyVisuals(Root r)
    {
        // this might be too much; commented out for now
        //ui.FlashRootReadyOverlay();
    }


    private float originalOrthoSize;

    [SerializeField]
    private float orthoBuffer;

    [SerializeField]
    private PlayerUI ui169;

    [SerializeField]
    private PlayerUI uiSmall169;

    [SerializeField]
    private PlayerUI ui89;

    internal PlayerUI ui { get; private set; }

    public void Setup(Player player, int numPlayers, int viewIndex)
    {
        myPlayer = player;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, MapData.Instance.GetPlayerFacing(player.playerIndex));

        shaker = Camera.GetComponent<EZCameraShake.CameraShaker>();

        ui169.gameObject.SetActive(false);
        ui89.gameObject.SetActive(false);
        uiSmall169.gameObject.SetActive(false);

        if (numPlayers == 1)
        {
            myCamera.rect = new Rect(0, 0, 1, 1);
            ui = ui169;
        }
        else if (numPlayers == 2)
        {
            switch(viewIndex)
            {
                case 0:
                    myCamera.rect = new Rect(0, 0, 0.5f, 1);
                    ui = ui89;
                    break;

                case 1:
                    myCamera.rect = new Rect(0.5f, 0, 0.5f, 1);
                    ui = ui89;
                    break;
            }
        }
        else if (numPlayers == 3)
        {
            switch (viewIndex)
            {
                case 0:
                    myCamera.rect = new Rect(0, 0, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;

                case 1:
                    myCamera.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;

                case 2:
                    myCamera.rect = new Rect(0.5f, 0, 0.5f, 1f);
                    ui = ui89;
                    break;
            }
        }
        else if (numPlayers == 4)
        {
            switch (viewIndex)
            {
                case 0:
                    myCamera.rect = new Rect(0, 0, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;

                case 1:
                    myCamera.rect = new Rect(0.5f, 0, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;

                case 2:
                    myCamera.rect = new Rect(0, 0.5f, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;

                case 3:
                    myCamera.rect = new Rect(0.5f, 0.5f, 0.5f, 0.5f);
                    ui = uiSmall169;
                    break;
            }
        }

        ui.gameObject.SetActive(true);

        ui.SetPlayer(player);
    }

    public Vector3 ScreenToWorldVector(Vector2 currentAimVector)
    {
        return transform.TransformDirection(currentAimVector);
    }

    private void Start()
    {
        originalOrthoSize = myCamera.orthographicSize;
    }

    private void Update()
    {

        if(viewingRoot)
        {
            /*
            Debug.DrawLine(myPlayer.ActiveRootBounds.center, myPlayer.ActiveRootBounds.center + Vector3.left * myPlayer.ActiveRootBounds.extents.x);
            Debug.DrawLine(myPlayer.ActiveRootBounds.center, myPlayer.ActiveRootBounds.center - Vector3.left * myPlayer.ActiveRootBounds.extents.x);
            Debug.DrawLine(myPlayer.ActiveRootBounds.center, myPlayer.ActiveRootBounds.center + Vector3.up * myPlayer.ActiveRootBounds.extents.y);
            Debug.DrawLine(myPlayer.ActiveRootBounds.center, myPlayer.ActiveRootBounds.center - Vector3.up * myPlayer.ActiveRootBounds.extents.y);
            */

            // move camera to match 
            Vector3 neededSize = transform.InverseTransformDirection(myPlayer.ActiveRootBounds.extents);
            neededSize.x = Mathf.Abs(neededSize.x);
            neededSize.y = Mathf.Abs(neededSize.y);
            if (neededSize.x / Screen.width > neededSize.y / Screen.height)
            {
                myCamera.orthographicSize = Mathf.Lerp(myCamera.orthographicSize, Mathf.Max(originalOrthoSize, orthoBuffer + neededSize.x / Screen.width * Screen.height), Time.deltaTime / 0.15f);
            }
            else
            {
                myCamera.orthographicSize = Mathf.Lerp(myCamera.orthographicSize, Mathf.Max(originalOrthoSize, orthoBuffer + neededSize.y), Time.deltaTime / 0.15f);
            }

            transform.position = Vector3.Lerp(transform.position, myPlayer.ActiveRootBounds.center, Time.deltaTime / 0.3f);
        }
        else
        {
            myCamera.orthographicSize = Mathf.Lerp(myCamera.orthographicSize, originalOrthoSize, Time.deltaTime / 0.15f);
            transform.position = Vector3.Lerp(transform.position, myPlayer.GetActiveTipPosition(), Time.deltaTime / 0.3f);
        }
    }

    private bool viewingRoot;

    internal void StartViewingRoot()
    {
        // TODO tip annotations
        viewingRoot = true;
        ui.SetShowTargetingIndicator(false);
    }

    internal void StopViewingRoot()
    {
        viewingRoot = false;
        ui.SetShowTargetingIndicator(true);
    }

    [SerializeField]
    private float maxOrtho;

    [SerializeField]
    private float minOrtho;

    [SerializeField]
    private float zoomSpeed;

    public void SetCameraZoom(float direction)
    {
        originalOrthoSize = Mathf.Clamp(originalOrthoSize + direction * zoomSpeed * Time.deltaTime, minOrtho, maxOrtho);
    }

}
