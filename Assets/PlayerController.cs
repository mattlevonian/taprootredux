﻿using System;
using System.Collections.Generic;
using UnityEngine;
using InControl;

/// <summary>
/// This class represents an actual "player", or more specifically 
/// the way they are interfacing with the game.
/// 
/// One exists for the keyboard, one exists for each controller attached.
/// During setup for a game, this gets attached to a Player object.
/// </summary>
public class PlayerController
{
    public const float DirectionDeadZone = 0.1f; //"directions" from joysticks will be ignored if their magnitude is less than this

    public const InputControlType ConnectAction = InControl.InputControlType.Action3;
    public const InputControlType MarkReadyAction = InControl.InputControlType.Action3;

    /// <summary>
    /// Each PlayerController has a device, and each device has one and only one PlayerController
    /// </summary>
    protected InputDevice myDevice;

    /// <summary>
    /// Will be null if the controller is not actively being used in the game.
    /// Stores the player this controller is attached to.
    /// </summary>
    protected Player myPlayer;


    public event Action<InputDevice> onControllerUsed;


    protected Vector2 currentAimVector;


    public PlayerController(InputDevice device)
    {
        myDevice = device;
    }

    public virtual void AttachToPlayer(Player newPlayer)
    {
        myPlayer = newPlayer;

        myPlayer.DeviceStyle = myDevice.DeviceStyle;
        myPlayer.DeviceClass = myDevice.DeviceClass;
    }
    public void DetachFromPlayer()
    {
        if(myPlayer != null)
        {
            GameManager.Instance.PlayerIsDisconnected(myPlayer);
        }
        myPlayer = null;
    }

    /// <summary>
    /// Here is where we translate controller button actions into function calls on the player.
    /// </summary>
    public virtual void Update()
    {
        if (myDevice.GetControl(MarkReadyAction).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.ToggleReady();
            }
        }
        if (myDevice.GetControl(ConnectAction).WasPressed)
        {
            if (myPlayer == null)
            {
                FireControllerUsedEvent();
                GameManager.Instance.TryAttachToPlayer(this);
            }
        }

        if (myDevice.GetControl(InputControlType.Start).WasPressed)
        {
            GameManager.Instance.TogglePauseMenu(this);
        }

        if (myDevice.GetControl(InputControlType.LeftStickUp).WasPressed || myDevice.GetControl(InputControlType.DPadUp).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.RequestMapUp();
            }
        }

        if (myDevice.GetControl(InputControlType.LeftStickDown).WasPressed || myDevice.GetControl(InputControlType.DPadDown).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.RequestMapDown();
            }
        }

        // TODO switch player color


        // game interactions
        if (myPlayer != null && myPlayer.viewContext != null)
        {
            currentAimVector = myPlayer.viewContext.ScreenToWorldVector(myDevice.Direction.Vector);
            myPlayer.SetTargetingDirection(myDevice.Direction.Vector);
            myPlayer.SetAbilityChooseDirection(myDevice.Direction.Vector);
        }

        if (myPlayer != null && myPlayer.viewContext != null)
        {
            myPlayer.SetCameraZoom(myDevice.RightStick.Vector.y);
        }

        // branch
        if (myDevice.GetControl(InputControlType.Action1).WasPressed)
        {
            if (myPlayer != null && myPlayer.viewContext != null)
            {
                myPlayer.StartGrowingActiveRoot();

                if (myPlayer.AllowedToBranch(currentAimVector))
                {
                    myPlayer.CreateBranchInWorldDirection(currentAimVector);
                }
                else
                {
                    // TODO negative feedback
                    SoundManager.Instance.PlaySound(SoundName.NotAllowedToBranch);
                    Debug.Log("not allowed to branch!");
                }
            }
        }

        // cut root
        if (myDevice.GetControl(InputControlType.Action2).WasPressed)
        {
            if (myPlayer != null)
            {
                if (myPlayer.AllowedToCut())
                {
                    myPlayer.StartCutting();
                }
                else
                {
                    // TODO negative feedback
                    SoundManager.Instance.PlaySound(SoundName.NotAllowedToCut);
                    Debug.Log("not allowed to cut!");
                }
            }
        }
        if (myDevice.GetControl(InputControlType.Action2).WasReleased)
        {
            if (myPlayer != null)
            {
                myPlayer.StopCutting();
            }
        }

        // switch roots
        if (myDevice.GetControl(InputControlType.LeftStickRight).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.RequestSwitchRootRight();
            }
        }
        if (myDevice.GetControl(InputControlType.DPadRight).WasPressed ||
            myDevice.GetControl(InputControlType.RightBumper).WasPressed ||
            myDevice.GetControl(InputControlType.RightTrigger).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.ForceSwitchRootRight();
            }
        }
        if (myDevice.GetControl(InputControlType.LeftStickLeft).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.RequestSwitchRootLeft();
            }
        }
        if (myDevice.GetControl(InputControlType.DPadLeft).WasPressed ||
            myDevice.GetControl(InputControlType.LeftBumper).WasPressed ||
            myDevice.GetControl(InputControlType.LeftTrigger).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.ForceSwitchRootLeft();
            }
        }

        // view root
        if (myDevice.GetControl(InputControlType.Action3).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.StartViewingRoot();
            }
        }
        if (myDevice.GetControl(InputControlType.Action3).WasReleased)
        {
            if (myPlayer != null)
            {
                myPlayer.StopViewingRoot();
            }
        }

        // view minimap
        if (myDevice.GetControl(InputControlType.Action4).WasPressed)
        {
            if (myPlayer != null)
            {
                myPlayer.StartViewingMinimap();
            }
        }
        if (myDevice.GetControl(InputControlType.Action4).WasReleased)
        {
            if (myPlayer != null)
            {
                myPlayer.StopViewingMinimap();
            }
        }
    }

    protected void FireControllerUsedEvent()
    {
        if (onControllerUsed != null)
        {
            onControllerUsed.Invoke(myDevice);
        }
    }
}

/// <summary>
/// Specialized version of the PlayerController that handles keyboard and mouse mappings
/// </summary>
public class PlayerController_Keyboard : PlayerController
{
    public InputDevice Device
    {
        get
        {
            return myDevice;
        }
    }


    public PlayerController_Keyboard() : base(new InputDevice("Keyboard"))
    {
    }

    public override void AttachToPlayer(Player newPlayer)
    {
        base.AttachToPlayer(newPlayer);

        myPlayer.DeviceClass = InputDeviceClass.Keyboard;
        myPlayer.DeviceStyle = InputDeviceStyle.Unknown;
    }

    public override void Update()
    {
        // non-game interactions

        // join / activate controller
        if(Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.E))
        {
            if (myPlayer == null)
            {
                FireControllerUsedEvent();
                GameManager.Instance.TryAttachToPlayer(this);
            }
            else
            {
                myPlayer.ToggleReady();
            }
        }

        // pause menu
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            GameManager.Instance.TogglePauseMenu(this);
        }

        // choose map
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
        {
            if (myPlayer != null)
            {
                myPlayer.RequestMapUp();
            }
        }
        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            if (myPlayer != null)
            {
                myPlayer.RequestMapDown();
            }
        }

        // disconnect input device
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            GameManager.Instance.GetPlayerControllerManager().OnControllerUnused(myDevice);
        }

        // game interactions

        if (myPlayer != null && myPlayer.viewContext != null)
        {
            Vector2 mousePos = Input.mousePosition;
            currentAimVector = myPlayer.viewContext.transform.InverseTransformDirection(myPlayer.viewContext.Camera.ScreenToWorldPoint(mousePos) - myPlayer.GetActiveTipPosition());
            currentAimVector.Normalize();
        }

        Vector2 currentChooseVector = Vector2.zero;

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
            currentChooseVector = Vector2.up;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            currentChooseVector = Vector2.left;
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            currentChooseVector = Vector2.right;
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
            currentChooseVector = Vector2.down;


        if (myPlayer != null && myPlayer.viewContext != null)
        {
            myPlayer.SetTargetingDirection(currentAimVector);
            myPlayer.SetAbilityChooseDirection(currentChooseVector);
            myPlayer.SetCameraZoom(Input.mouseScrollDelta.y);
        }

        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            if (myPlayer != null && myPlayer.viewContext != null)
            {
                currentAimVector = myPlayer.viewContext.ScreenToWorldVector(currentAimVector);

                myPlayer.StartGrowingActiveRoot();

                if (myPlayer.AllowedToBranch(currentAimVector))
                {
                    myPlayer.CreateBranchInWorldDirection(currentAimVector);
                }
                else
                {
                    // TODO negative feedback
                    SoundManager.Instance.PlaySound(SoundName.NotAllowedToBranch);
                    Debug.Log("not allowed to branch!");
                }
            }
        }

        // cut root
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (myPlayer != null)
            {
                if (myPlayer.AllowedToCut())
                {
                    myPlayer.StartCutting();
                }
                else
                {
                    // TODO negative feedback
                    SoundManager.Instance.PlaySound(SoundName.NotAllowedToCut);
                    Debug.Log("not allowed to cut!");
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            if (myPlayer != null)
            {
                myPlayer.StopCutting();
            }
        }

        // switch roots
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (myPlayer != null)
            {
                myPlayer.RequestSwitchRootRight();
            }
        }
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (myPlayer != null)
            {
                myPlayer.RequestSwitchRootLeft();
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            if (myPlayer != null)
            {
                myPlayer.ForceSwitchRootRight();
            }
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (myPlayer != null)
            {
                myPlayer.ForceSwitchRootLeft();
            }
        }

        // view root
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (myPlayer != null)
            {
                myPlayer.StartViewingRoot();
            }
        }
        if (Input.GetKeyUp(KeyCode.E))
        {
            if (myPlayer != null)
            {
                myPlayer.StopViewingRoot();
            }
        }

        // view minimap
        if (Input.GetKeyDown(KeyCode.F))
        {
            if (myPlayer != null)
            {
                myPlayer.StartViewingMinimap();
            }
        }
        if (Input.GetKeyUp(KeyCode.F))
        {
            if (myPlayer != null)
            {
                myPlayer.StopViewingMinimap();
            }
        }
    }
}