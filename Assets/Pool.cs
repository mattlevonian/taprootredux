﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Attached to any pool that a branch can enter.
/// Extended to implement ResourcePool, FirePool, and PoisonPool.
/// </summary>
public class Pool : Obstacle
{
    public UniqueID id;

    /// <summary>
    /// Called when a branch starts being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the new branch</param>
    public virtual void AddBranch(Branch b)
    {

    }

    /// <summary>
    /// Called when a branch stops being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the branch being disconnected</param>
    public virtual void RemoveBranch(Branch b)
    {

    }

    public static Pool Find(int poolID)
    {
        foreach(Pool p in FindObjectsOfType<Pool>())
        {
            if(p.id.ID == poolID)
            {
                return p;
            }
        }
        return null;
    }

    private void OnValidate()
    {
        id = new UniqueID();
    }
}

[System.Serializable]
public class UniqueID
{
    public int ID = 2;

    public static List<UniqueID> uids = new List<UniqueID>();

    public UniqueID()
    {
        //Debug.Log("Creating unique ID: " + uids.Count);
        int max = -1;
        foreach (UniqueID uid in uids)
        {
            //Debug.Log(uid.ID);
            if (uid != this && max < uid.ID)
            {
                max = uid.ID;
            }
        }
        //Debug.Log("final: " + (max + 1).ToString());
        ID = max + 1;
        if(!uids.Contains(this))
        {
            uids.Add(this);
        }
    }
}

