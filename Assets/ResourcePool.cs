﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePool : Pool, IPunObservable
{
    [SerializeField]
    private float startingAmount;

    private float currentAmount;

    [SerializeField]
    private Renderer myRenderer;

    internal float FractionLeft
    {
        get
        {
            return currentAmount / startingAmount;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(currentAmount);
        }
        else
        {
            currentAmount = (float)stream.ReceiveNext();
        }
    }


    private List<Branch> feeders;

    private Material myMaterial;

    /// <summary>
    /// Called when a branch starts being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the new branch</param>
    public override void AddBranch(Branch b)
    {
        base.AddBranch(b);

        feeders.Add(b);
    }

    /// <summary>
    /// Called when a branch stops being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the branch being disconnected</param>
    public override void RemoveBranch(Branch b)
    {
        base.RemoveBranch(b);

        feeders.Remove(b);
    }

    private void Start()
    {
        feeders = new List<Branch>();
        currentAmount = startingAmount;
        myMaterial = myRenderer.material;
    }

    private void FixedUpdate()
    {
        // TODO experiment with sending "pulses" rather than a continuous stream

        if (!Mathf.Approximately(currentAmount, 0))
        {
            float totalFeedAmount = 0;

            foreach(Branch b in feeders)
            {
                totalFeedAmount += b.DrainRate * Time.deltaTime;
            }

            float fraction = 1;

            if (currentAmount < totalFeedAmount)
            {
                fraction = currentAmount / totalFeedAmount;
            }

            foreach (Branch b in feeders)
            {
                float feedAmount = b.DrainRate * Time.deltaTime * fraction;

                if (b.AddResources(feedAmount, b)) // TODO maybe do this check earlier when calculating total?
                {
                    currentAmount -= feedAmount;
                }
            }

            myMaterial.SetFloat("_Blend", FractionLeft);
        }
    }
}
