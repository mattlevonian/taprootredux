﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    private Player myPlayer;

    [SerializeField]
    private Text playerNameText;

    [SerializeField]
    private Text playerScoreText;

    public void SetInfo(Player player)
    {
        if (myPlayer != null)
        {
            myPlayer.onInfoUpdate -= UpdateInfo;
        }

        myPlayer = player;
        myPlayer.onInfoUpdate += UpdateInfo;

        UpdateInfo();
    }

    private void UpdateInfo()
    {
        playerNameText.text = myPlayer.playerName;
        playerScoreText.text = Mathf.RoundToInt(GameManager.Instance.GlobalScoreKeeper.GetScoreByPlayerIndex(myPlayer.playerIndex)).ToString();
    }

    private void OnDestroy()
    {
        if (myPlayer != null)
        {
            myPlayer.onInfoUpdate -= UpdateInfo;
        }
    }
}
