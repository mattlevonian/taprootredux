﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameJoinUI : MonoBehaviour
{
    [SerializeField]
    private JoinButton buttonPrefab;
    [SerializeField]
    private Transform buttonParent;

    private Action<bool> hostGameCallback;
    private Action hostTutorialCallback;

    [SerializeField]
    private Button onlineHostGameButton;

    [SerializeField]
    private Button offlineHostGameButton;

    private void Start()
    {
        onlineHostGameButton.interactable = false;
        offlineHostGameButton.interactable = false;
    }

    public void SetHostCallback(Action<bool> callback, Action tutorialCallback)
    {
        hostGameCallback = callback;
        hostTutorialCallback = tutorialCallback;

        onlineHostGameButton.interactable = canHost && onlinePlayEnabled && (hostGameCallback != null);
        offlineHostGameButton.interactable = canHost && (hostGameCallback != null);
    }

    private bool onlinePlayEnabled;

    public void SetOnlinePlayEnabled(bool on)
    {
        onlinePlayEnabled = on;

        onlineHostGameButton.interactable = canHost && onlinePlayEnabled && (hostGameCallback != null);
        offlineHostGameButton.interactable = canHost && (hostGameCallback != null);
    }

    private bool canHost;

    public void SetCanHost(bool newCanHost)
    {
        canHost = newCanHost;

        onlineHostGameButton.interactable = canHost && onlinePlayEnabled && (hostGameCallback != null);
        offlineHostGameButton.interactable = canHost && (hostGameCallback != null);
    }

    public void HostGame()
    {
        hostGameCallback.Invoke(true);
    }

    public void HostOfflineGame()
    {
        hostGameCallback.Invoke(false);
    }

    public void HostTutorialGame()
    {
        hostTutorialCallback.Invoke();
    }

    public void UpdateListOfRooms(RoomInfo[] rooms, Action<string> joinAction)
    {
        //Destroy any exising room buttons
        foreach (JoinButton b in buttonParent.GetComponentsInChildren<JoinButton>())
        {
            Destroy(b.gameObject);
        }

        //Populate the panel with the new room info
        foreach (RoomInfo room in rooms)
        {
            JoinButton button = Instantiate(buttonPrefab, buttonParent.transform, false);
            button.Init(room, joinAction);
        }
    }


    // focus management

    [SerializeField]
    private GameObject defaultSelectedObject;

    public void Focus()
    {
        EventSystem.current.SetSelectedGameObject(defaultSelectedObject);
    }

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            Focus();
        }
    }
}
