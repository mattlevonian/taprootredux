﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RoomLobbyUI : MonoBehaviour
{
    [SerializeField]
    private PlayerLabel playerUIPrefab;

    [SerializeField]
    private RectTransform playersList;

    [SerializeField]
    private Button startButton;

    [SerializeField]
    private GameObject noStartMessage;

    [SerializeField]
    private Text roomNameField;

    public void SetAbleToStart(bool startable)
    {
        startButton.interactable = startable;
    }

    public void StartGame()
    {
        GameManager.Instance.StartGame();
    }

    public void LeaveRoom()
    {
        GameManager.Instance.LeaveRoom();
    }

    public void UpdatePlayerList()
    {
        if (playersList != null)
        {
            foreach (PlayerLabel g in playersList.GetComponentsInChildren<PlayerLabel>())
            {
                Destroy(g.gameObject);
            }

            // show activated devices
            foreach (Player player in Player.Players)
            {
                if (player != null)
                {
                    PlayerLabel label = Instantiate(playerUIPrefab, playersList, false);
                    label.SetInfo(player);
                }
            }
        }
    }

    public void HideStartButton(bool hide)
    {
        noStartMessage.SetActive(hide);
        startButton.gameObject.SetActive(!hide);
    }

    private void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        // deal with case where master client leaves and we become master client
        Debug.Log("Master client changed!");
        HideStartButton(!PhotonNetwork.isMasterClient);
    }

    private void OnEnable()
    {
        UpdateMapSelection();
        roomNameField.text = GameManager.Instance.GetRoomName();
    }

    // map management stuff

    [Serializable]
    private struct MapMetaData
    {
        public Sprite overviewImage;
        public string mapName;
        public int sceneIndex;
        public int maxPlayers;
    }
    [Space(12)]
    [SerializeField]
    private MapMetaData[] maps;

    private int mapIndex;

    internal int CurrentMapBuildIndex
    {
        get
        {
            return maps[mapIndex].sceneIndex;
        }
    }

    [SerializeField]
    private Image mapImage;
    [SerializeField]
    private Text mapNameField;

    public void MapUp()
    {
        mapIndex += 1;

        UpdateMapSelection();
    }

    public void MapDown()
    {
        mapIndex -= 1;

        UpdateMapSelection();
    }

    private void UpdateMapSelection()
    {
        mapIndex = Mathf.RoundToInt(Mathf.Repeat(mapIndex, maps.Length));

        mapImage.sprite = maps[mapIndex].overviewImage;
        mapNameField.text = maps[mapIndex].mapName;
    }


    // focus management

    [Space(12)]
    [SerializeField]
    private GameObject defaultSelectedObject;

    public void Focus()
    {
        EventSystem.current.SetSelectedGameObject(defaultSelectedObject);
    }

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            Focus();
        }
    }
}
