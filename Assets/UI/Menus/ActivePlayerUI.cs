﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

public class ActivePlayerUI : MonoBehaviour
{
    [SerializeField]
    private GameObject deviceUIPrefab;

    [SerializeField]
    private RectTransform devicesList;

    private List<GameObject> uiDevices;

    // TODO show join/activate button prompts for all the attached devices (even if they aren't active)
    // TODO controls screen

    public void UpdateDevicesUI(Dictionary<InputDevice, PlayerController> controllers, List<InputDevice> activatedDevices)
    {
        if (uiDevices != null)
        {
            foreach (GameObject g in uiDevices)
            {
                Destroy(g);
            }
            uiDevices.Clear();
        }

        uiDevices = new List<GameObject>();

        // show activated devices
        foreach (InputDevice device in activatedDevices)
        {
            GameObject deviceUI = Instantiate(deviceUIPrefab, devicesList, false);
            deviceUI.GetComponentInChildren<Text>().text = device.Name;
            uiDevices.Add(deviceUI);
        }
    }

}
