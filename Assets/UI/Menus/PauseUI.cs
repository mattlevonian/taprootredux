﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PauseUI : MonoBehaviour
{
    [SerializeField]
    private GameObject defaultSelectedObject;

    [SerializeField]
    private GameObject endGameButton;

    [SerializeField]
    private GameObject leaveRoomButton;

    public void Focus()
    {
        EventSystem.current.SetSelectedGameObject(defaultSelectedObject);
    }

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            Focus();
        }
    }

    private PlayerController myController;

    public void SetController(PlayerController pausingController)
    {
        myController = pausingController;
    }

    private void OnEnable()
    {
        endGameButton.SetActive(PhotonNetwork.isMasterClient);
        leaveRoomButton.SetActive(PhotonNetwork.inRoom);
    }

    // actions

    public void GoBack()
    {
        GameManager.Instance.TogglePauseMenu(myController);
    }

    public void EndGame()
    {
        GameManager.Instance.EndGame();
        GameManager.Instance.TogglePauseMenu(myController);
    }

    public void LeaveGame()
    {
        GameManager.Instance.LeaveRoom();
        GameManager.Instance.TogglePauseMenu(myController);
    }
}
