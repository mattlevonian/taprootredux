﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PostGameUI : MonoBehaviour {
    [SerializeField]
    private PlayerScore playerUIPrefab;

    [SerializeField]
    private RectTransform playersList;

    [SerializeField]
    private Button restartButton;

    [SerializeField]
    private GameObject noStartMessage;

    public void ReturnToLobby()
    {
        GameManager.Instance.ReturnToLobby();
    }

    public void LeaveRoom()
    {
        GameManager.Instance.LeaveRoom();
    }

    private void OnEnable()
    {
        HideRestartButton(!PhotonNetwork.isMasterClient);
    }

    private void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        // deal with case where master client leaves and we become master client
        Debug.Log("Master client changed!");
        HideRestartButton(!PhotonNetwork.isMasterClient);
    }

    public void UpdatePlayerList()
    {
        foreach (PlayerScore g in playersList.GetComponentsInChildren<PlayerScore>())
        {
            Destroy(g.gameObject);
        }
        
        // TODO sort by score

        // show all players
        foreach (Player player in Player.Players)
        {
            if (player != null)
            {
                PlayerScore label = Instantiate(playerUIPrefab, playersList, false);
                label.SetInfo(player);
            }
        }
    }

    public void HideRestartButton(bool hide)
    {
        noStartMessage.SetActive(hide);
        restartButton.gameObject.SetActive(!hide);
    }

    // focus management

    [SerializeField]
    private GameObject defaultSelectedObject;

    public void Focus()
    {
        EventSystem.current.SetSelectedGameObject(defaultSelectedObject);
    }

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            Focus();
        }
    }
}
