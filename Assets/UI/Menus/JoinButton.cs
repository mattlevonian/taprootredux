﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoinButton : MonoBehaviour
{
    private string TargetRoomName;
    private Action<string> JoinAction;

    public void Init(RoomInfo room, Action<string> joinAction)
    {
        JoinAction = joinAction;
        TargetRoomName = room.Name;
        GetComponentInChildren<Text>().text = TargetRoomName;
    }

    public void Join()
    {
        JoinAction.Invoke(TargetRoomName);
    }
}
