﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLabel : MonoBehaviour
{
    private Player myPlayer;

    [SerializeField]
    private Toggle readyToggle;

    [SerializeField]
    private Text playerNameText;

    [SerializeField]
    private Image readyControlImage;

    [SerializeField]
    private Image colorImage;

    public void SetInfo(Player player)
    {
        if(myPlayer != null)
        {
            myPlayer.onReady -= SetReady;
            myPlayer.onInfoUpdate -= UpdateInfo;
        }

        myPlayer = player;
        myPlayer.onReady += SetReady;
        myPlayer.onInfoUpdate += UpdateInfo;

        UpdateInfo();
    }

    private void SetReady(bool ready)
    {
        readyToggle.isOn = ready;
    }

    private void UpdateInfo()
    {
        playerNameText.text = myPlayer.playerName;

        SetReady(myPlayer.isReady);

        colorImage.color = myPlayer.playerColor;

        if (myPlayer.isLocal)
        {
            readyControlImage.gameObject.SetActive(true);
            readyControlImage.sprite = GameManager.Instance.GetButtonMapManager().GetSprite(myPlayer.DeviceClass, myPlayer.DeviceStyle, PlayerController.MarkReadyAction);
        }
        else
        {
            readyControlImage.gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        if(myPlayer != null)
        {
            myPlayer.onReady -= SetReady;
            myPlayer.onInfoUpdate -= UpdateInfo;
        }
    }
}
