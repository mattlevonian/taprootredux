﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RootIndicator : MonoBehaviour
{
    [SerializeField]
    private Sprite[] timerSprites;

    [SerializeField]
    private Image timerImage;

    [SerializeField]
    private Image iconImage;

    [SerializeField]
    private Image selectionImage;

    [SerializeField]
    private Image bgImage;

    [SerializeField]
    private Color okColor;

    [SerializeField]
    private Color deadColor;

    [SerializeField]
    private Color fireColor;

    internal Color playerColor;

    private void Start()
    {
        timerImage.enabled = false;

        selectionImage.color = playerColor;
        iconImage.color = playerColor;

        OnRootDies();
    }

    public void OnRootDies()
    {
        // turn gray and faded
        bgImage.color = deadColor;

        if (pulseRoutine != null)
        {
            StopCoroutine(pulseRoutine);
            pulseRoutine = null;
            LeanTween.cancel(iconImage.gameObject);
            iconImage.transform.localScale = Vector3.one;
        }
    }

    public void OnRootStartsRespawning(float respawnTime)
    {
        // show ring timer
        StartCoroutine(RespawnTimer(respawnTime));
    }

    public void OnRootReadyToGrow()
    {
        // turn green and start pulsing
        bgImage.color = okColor;

        if (pulseRoutine == null)
        {
            pulseRoutine = StartCoroutine(PulseRoutine());
        }
    }

    public void OnRootStartsGrowing()
    {
        // turn green and stop pulsing
        bgImage.color = okColor;

        if(pulseRoutine != null)
        {
            StopCoroutine(pulseRoutine);
            pulseRoutine = null;
            LeanTween.cancel(iconImage.gameObject);
            iconImage.transform.localScale = Vector3.one;
        }
    }

    public void OnRootCatchFire()
    {
        // turn red and start pulsing
        bgImage.color = fireColor;

        if (pulseRoutine == null)
        {
            pulseRoutine = StartCoroutine(PulseRoutine());
        }
    }

    public void SetSelected(bool selected)
    {
        selectionImage.enabled = selected;
    }

    private IEnumerator RespawnTimer(float respawnTime)
    {
        float segmentTime = respawnTime / (timerSprites.Length + 1);

        WaitForSeconds wait = new WaitForSeconds(segmentTime);

        // first frame is no image
        timerImage.enabled = false;
        yield return wait;
        timerImage.enabled = true;

        for(int i = 0; i < timerSprites.Length; i++)
        {
            timerImage.sprite = timerSprites[i];
            yield return wait;
        }

        // at this point the root should be respawned, so we hide the image
        timerImage.enabled = false;
    }

    [SerializeField]
    private float pulseRate;

    private Coroutine pulseRoutine;
    private IEnumerator PulseRoutine()
    {
        while (true)
        {
            bool pulseDone = false;
            var desc = LeanTween.scale(iconImage.gameObject, Vector3.one * 1.25f, pulseRate / 2f).setEase(LeanTweenType.easeInOutQuad);
            desc.setOnComplete(() => { pulseDone = true; });
            while (!pulseDone)
            {
                yield return null;
            }

            pulseDone = false;
            desc = LeanTween.scale(iconImage.gameObject, Vector3.one, pulseRate / 2f).setEase(LeanTweenType.easeInOutQuad);
            desc.setOnComplete(() => { pulseDone = true; });
            while (!pulseDone)
            {
                yield return null;
            }
        }
    }
}
