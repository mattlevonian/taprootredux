﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class handles the panel at the bottom of the screen that displays the status of the player's roots
/// </summary>
public class RootUI : MonoBehaviour
{
    [SerializeField]
    private RootIndicator indicatorPrefab;

    [SerializeField]
    private Color fireOverlayColor = Color.red;

    [SerializeField]
    private Color readyToGrowOverlayColor = Color.green;

    [SerializeField]
    private float overlayFlashOnTime = 0.125f;

    [SerializeField]
    private float overlayFlashOffTime = 0.725f;

    [SerializeField]
    private Image lowerGradient;

    private Player myPlayer;

    private RootIndicator[] indicators;

    public void Setup(Player p)
    {
        myPlayer = p;

        myPlayer.onRootDies += OnRootDies;
        myPlayer.onRootStartsRespawning += OnRootStartsRespawning;
        myPlayer.onRootStartsGrowing += OnRootStartsGrowing;
        myPlayer.onRootCatchFire += OnRootCatchFire;
        myPlayer.onRootReadyToGrow += OnRootReadyToGrow;
        myPlayer.onChangeActiveRoot += OnChangeActiveRoot;


        // populate row of indicators
        indicators = new RootIndicator[MapData.Instance.NumRootSlots];

        for (int i = 0; i < indicators.Length; i++)
        {
            indicators[i] = Instantiate(indicatorPrefab, this.transform, false);
            indicators[i].playerColor = myPlayer.playerColor;
        }
    }

    private void OnDestroy()
    {
        myPlayer.onRootDies -= OnRootDies;
        myPlayer.onRootStartsRespawning -= OnRootStartsRespawning;
        myPlayer.onRootStartsGrowing -= OnRootStartsGrowing;
        myPlayer.onRootCatchFire -= OnRootCatchFire;
        myPlayer.onRootReadyToGrow -= OnRootReadyToGrow;
        myPlayer.onChangeActiveRoot -= OnChangeActiveRoot;
    }

    private void OnRootDies(int rootIndex)
    {
        SoundManager.Instance.PlaySound(SoundName.RootDies);
        indicators[rootIndex].OnRootDies();
    }

    private void OnRootStartsRespawning(int rootIndex)
    {
        SoundManager.Instance.PlaySound(SoundName.RootStartsRespawning);
        indicators[rootIndex].OnRootStartsRespawning(myPlayer.GetRootRespawnTime(rootIndex));
    }

    private void OnRootReadyToGrow(int rootIndex)
    {
        SoundManager.Instance.PlaySound(SoundName.RootReadyToGrow);
        indicators[rootIndex].OnRootReadyToGrow();

        FlashScreenOverlay(readyToGrowOverlayColor);
    }

    private void OnRootStartsGrowing(int rootIndex)
    {
        SoundManager.Instance.PlaySound(SoundName.RootStartsGrowing);
        indicators[rootIndex].OnRootStartsGrowing();
    }

    private void OnRootCatchFire(int rootIndex)
    {
        SoundManager.Instance.PlaySound(SoundName.RootCatchesFire);
        indicators[rootIndex].OnRootCatchFire();

        FlashScreenOverlay(fireOverlayColor);
    }

    Coroutine flashingRoutine;
    private void FlashScreenOverlay(Color c)
    {
        if(flashingRoutine != null)
        {
            StopCoroutine(flashingRoutine);
        }

        flashingRoutine = StartCoroutine(FlashScreenOverlayRoutine(c));
    }

    private IEnumerator FlashScreenOverlayRoutine(Color c)
    {
        lowerGradient.gameObject.SetActive(true);
        LeanTween.cancel(lowerGradient.gameObject);
        Action<Color> d = (Color f) => { lowerGradient.color = f; };
        LeanTween.value(lowerGradient.gameObject, d, new Color(c.r, c.g, c.b, 0), c, overlayFlashOnTime);
        yield return new WaitForSeconds(overlayFlashOnTime);
        LeanTween.value(lowerGradient.gameObject, d, c, new Color(c.r, c.g, c.b, 0), overlayFlashOffTime);
        lowerGradient.gameObject.SetActive(false);
    }

    private void OnChangeActiveRoot(int newIndex)
    {
        for (int i = 0; i < indicators.Length; i++)
        {
            indicators[i].SetSelected(i == newIndex);
        }
    }
}
