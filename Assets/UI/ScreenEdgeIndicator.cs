﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenEdgeIndicator : MonoBehaviour
{
    [SerializeField]
    private Image[] border;

    [SerializeField]
    private Image icon;

    private Branch myBranch;

    internal ViewContext myContext;

    [SerializeField]
    private float pixelBuffer;

    public void Setup(Branch b)
    {
        myBranch = b;

        foreach(Image i in border)
        {
            i.color = myBranch.myRoot.player.playerColor;
        }
    }

    private void Update()
    {
        // TODO indicators combine when they are too close to each other
        // TODO indicators change size to indicate distance (or add arrows)

        if(myBranch == null || !myBranch.isGrowing)
        {
            Destroy(this.gameObject);
        }
        else
        {
            // point at branch
            Vector2 viewPortBuffer = myContext.Camera.ScreenToViewportPoint(Vector2.one * pixelBuffer);

            Vector2 screenPos = myContext.Camera.WorldToViewportPoint(myBranch.EndPositionInWorld);
            screenPos.x = Mathf.Clamp(screenPos.x, viewPortBuffer.x, 1 - viewPortBuffer.x);
            screenPos.y = Mathf.Clamp(screenPos.y, viewPortBuffer.x, 1 - viewPortBuffer.y);

            Vector3 pos = myContext.Camera.ViewportToWorldPoint(screenPos);
            pos.z = myContext.ui.transform.position.z;
            transform.position = pos;

            transform.rotation = Quaternion.LookRotation(Vector3.forward, myBranch.EndPositionInWorld - transform.position);
        }
    }
}
