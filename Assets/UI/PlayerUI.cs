﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class controls elements of the in-game UI that are specific to a local player
/// </summary>
public class PlayerUI : MonoBehaviour
{
    [SerializeField]
    private Text gameTimerField;

    private Player myPlayer;

    [SerializeField]
    private PlayerScoreUI scoreUI;

    [SerializeField]
    private RootUI rootUI;

    [SerializeField]
    private Map[] maps;

    [SerializeField]
    private Map largeMap;

    [SerializeField]
    private PlayerControlImage[] controllerPrompts;

    [SerializeField]
    private Image cutPromptImage;

    [SerializeField]
    private Image cutPromptRing;

    [SerializeField]
    private RectTransform targetingIndicator;

    [SerializeField]
    private RectTransform tipHud;

    [SerializeField]
    private AbilityChooser abilityChooser;

    [SerializeField]
    private Image rootAbility;

    [SerializeField]
    private Sprite[] abilitySprites;

    private void OnSwitchActiveRoot(int rootIndex)
    {
        rootAbility.sprite = abilitySprites[myPlayer.ActiveRootAbility];

        StopFireOverlay();
    }


    [SerializeField]
    private Image fireOverlay;

    [SerializeField]
    private Image resourcesOverlay;

    [SerializeField]
    private Image rootReadyOverlay;

    private Coroutine fireOverlayCoroutine;
    public void FlashFireOverlay(bool loop)
    {
        FlashOverlay(fireOverlay, Color.red, 0.25f, ref fireOverlayCoroutine, () => { fireOverlayCoroutine = null; }, loop);
    }
    public void StopFireOverlay()
    {
        if (fireOverlayCoroutine != null)
        {
            StopCoroutine(fireOverlayCoroutine);
            fireOverlayCoroutine = null;
        }
        fireOverlay.gameObject.SetActive(false);
    }
    
    private Coroutine resourcesOverlayCoroutine;
    public void FlashResourcesOverlay()
    {
        if(fireOverlayCoroutine == null && rootReadyOverlayCoroutine == null)
        FlashOverlay(resourcesOverlay, Color.blue, 0.25f, ref resourcesOverlayCoroutine, () => { resourcesOverlayCoroutine = null; });
    }

    private Coroutine rootReadyOverlayCoroutine;
    public void FlashRootReadyOverlay()
    {
        if(fireOverlayCoroutine == null)
        FlashOverlay(rootReadyOverlay, Color.green, 0.25f, ref rootReadyOverlayCoroutine, () => { rootReadyOverlayCoroutine = null; });
    }

    private void FlashOverlay(Image overlayImage, Color c, float flashTime, ref Coroutine coroutine, Action callback, bool loop = false)
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        coroutine = StartCoroutine(FlashOverlayRoutine(overlayImage, c, flashTime, callback, loop));
    }
    private IEnumerator FlashOverlayRoutine(Image overlayImage, Color c, float flashTime, Action callback, bool loop)
    {
        do
        {
            overlayImage.gameObject.SetActive(true);
            Action<Color> d = (Color f) => { overlayImage.color = f; };
            LeanTween.cancel(overlayImage.gameObject);
            LeanTween.value(overlayImage.gameObject, d, new Color(c.r, c.g, c.b, 0), c, flashTime);
            yield return new WaitForSeconds(flashTime);
            LeanTween.value(overlayImage.gameObject, d, c, new Color(c.r, c.g, c.b, 0), flashTime);
            yield return new WaitForSeconds(flashTime);
            overlayImage.gameObject.SetActive(false);
        } while (loop);
        callback.Invoke();
    }


    // TODO  aiming indicator

    [SerializeField, Tooltip("A branch outside the screen and within this distance will result in a screen edge indicator")]
    private float maxDistanceForIndicator;

    [SerializeField]
    private ScreenEdgeIndicator screenEdgeIndicatorPrefab;

    private Dictionary<int, ScreenEdgeIndicator> indicators = new Dictionary<int, ScreenEdgeIndicator>();

    public void SetPlayer(Player player)
    {
        myPlayer = player;
        myPlayer.onChangeActiveRoot += OnSwitchActiveRoot;
        myPlayer.onRootStartsGrowing += OnSwitchActiveRoot;
        myPlayer.onRootDies += OnSwitchActiveRoot;

        rootUI.Setup(player);
        scoreUI.Setup(player);

        largeMap.gameObject.SetActive(false);

        foreach(PlayerControlImage i in controllerPrompts)
        {
            i.Setup(player);
        }
    }

    private void OnDestroy()
    {
        myPlayer.onChangeActiveRoot -= OnSwitchActiveRoot;
        myPlayer.onRootStartsGrowing -= OnSwitchActiveRoot;
        myPlayer.onRootDies -= OnSwitchActiveRoot;
    }

    private void Start()
    {
        cutPromptImage.enabled = false;
        cutPromptRing.enabled = false;
    }

    private void Update()
    {
        float timeRemaining = GameManager.Instance.GameTimeLeft;
        int minutes = Mathf.FloorToInt(timeRemaining / 60);
        int seconds = Mathf.FloorToInt(timeRemaining % 60);
        gameTimerField.text = minutes.ToString() + ":" + seconds.ToString("D2");

        // move targeting indicator over tip (may not be in center of the screen all the time)
        Vector3 worldPos = myPlayer.GetActiveTipPosition();
        tipHud.position = worldPos + Vector3.back;

        targetingIndicator.gameObject.SetActive(myPlayer.ActiveTipIsGrowing);

        foreach(Player p in Player.Players)
        {
            if (p != null && p != myPlayer)
            {
                foreach (Branch b in p.GrowingBranches)
                {
                    if (b.myRoot.hasStarted && b.isGrowing)
                    {
                        if (BranchOutsideOfScreen(b) && DistanceOutsideOfScreen(b.EndPositionInWorld) < maxDistanceForIndicator)
                        {
                            if (!indicators.ContainsKey(b.GUID))
                            {
                                ScreenEdgeIndicator i = Instantiate(screenEdgeIndicatorPrefab, this.transform, false);
                                i.myContext = myPlayer.viewContext;
                                i.Setup(b);
                                indicators.Add(b.GUID, i);
                            }
                        }
                        else
                        {
                            if (indicators.ContainsKey(b.GUID))
                            {
                                Destroy(indicators[b.GUID].gameObject);
                                indicators.Remove(b.GUID);
                            }
                        }
                    }
                }
            }
        }

        List<int> expiredIndicators = new List<int>();
        foreach(int i in indicators.Keys)
        {
            if(indicators[i] == null)
            {
                expiredIndicators.Add(i);
            }
        }
        foreach(int i in expiredIndicators)
        {
            indicators.Remove(i);
        }
    }

    public bool BranchOutsideOfScreen(Branch b)
    {
        Vector3 screenPoint = myPlayer.viewContext.Camera.WorldToViewportPoint(b.EndPositionInWorld);
        return screenPoint.x < 0 || screenPoint.x > 1 || screenPoint.y < 0 || screenPoint.y > 1;
        //Debug.DrawLine(transform.position, b.EndPositionInWorld, retval? Color.red :Color.white);
        //return retval;
    }

    private float DistanceOutsideOfScreen(Vector3 pos)
    {
        return Vector3.Distance(transform.position, pos);
    }

    /// <summary>
    /// Updates the UI to show where the player is aiming
    /// </summary>
    /// <param name="screenDirection">A vector where "up" is towards the top of the player's screen.
    ///     A very low or 0 magnitude vector means the indicator should be hidden.</param>
    public void SetTargetingDirection(Vector2 screenDirection)
    {
        if (screenDirection.magnitude > PlayerController.DirectionDeadZone)
        {
            targetingIndicator.gameObject.SetActive(true);
            targetingIndicator.localRotation = Quaternion.LookRotation(Vector3.forward, screenDirection);
        }
        else
        {
            targetingIndicator.gameObject.SetActive(false);
        }
    }

    public void SetShowTargetingIndicator(bool on)
    {
        targetingIndicator.gameObject.SetActive(on);
    }

    public void ResetAbilityChooser()
    {
        abilityChooser.ResetChoosing();
    }

    public void SetShowAbilityChooser(bool on)
    {
        if (on)
        {
            abilityChooser.Show();
        }
        else
        {
            abilityChooser.Hide();
        }
    }

    /// <summary>
    /// Sets the direction the player is choosing for the ability menu
    /// </summary>
    /// <param name="screenDirection">A vector where "up" is towards the top of the player's screen.
    ///     A very low or 0 magnitude vector means the indicator should be hidden.</param>
    public void SetAbilityChooseDirection(Vector2 screenDirection)
    {
        abilityChooser.SetAbilityChooseDirection(screenDirection);
    }

    /// <summary>
    /// Returns the section the player has selected from the ability menu
    /// </summary>
    /// <returns>An index, where 0 = none, and 1-n = abilities starting at the top and going clockwise</returns>
    public int GetAbilityIndex()
    {
        return abilityChooser.GetAbilityIndex();
    }

    // cutting tooltip
    public void StartCutting(float timeToCut)
    {
        if (cuttingRoutine == null)
        {
            cutPromptImage.enabled = true;
            cutPromptRing.enabled = true;

            cuttingRoutine = StartCoroutine(CutTimer(timeToCut));
        }
    }
    public void StopCutting()
    {
        if (cuttingRoutine != null)
        {
            cutPromptImage.enabled = false;
            cutPromptRing.enabled = false;

            StopCoroutine(cuttingRoutine);
            cuttingRoutine = null;
        }
    }
    private Coroutine cuttingRoutine;
    private IEnumerator CutTimer(float timeToCut)
    {
        float time = 0;
        while(time < timeToCut)
        {
            // show ring around button image
            cutPromptRing.fillAmount = time / timeToCut;
            time += Time.deltaTime;
            yield return null;
        }

        cutPromptImage.enabled = false;
        cutPromptRing.enabled = false;
    }

    internal void ShowMinimap()
    {
        largeMap.gameObject.SetActive(true);
    }

    internal void HideMinimap()
    {
        largeMap.gameObject.SetActive(false);
    }
}
