﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBar : MonoBehaviour
{
    [SerializeField]
    private RectTransform bg;

    [SerializeField]
    private RectTransform bar;

    internal void UpdateScore(float p)
    {
        bar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, p * bg.rect.width);
    }

    internal void SetColor(Color playerColor)
    {
        bar.GetComponent<Image>().color = playerColor;
    }
}
