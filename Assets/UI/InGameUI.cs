﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class controls the elements of the UI that aren't per-player
/// </summary>
public class InGameUI : MonoBehaviour
{
    [SerializeField]
    private GameObject countdownPanel;

    [SerializeField]
    private Text countdownField;

    [SerializeField]
    private GameObject[] screenDividers;

    public void Init(int numLocalPlayers)
    {
        for(int i = 0; i < screenDividers.Length; i++)
        {
            if(i == numLocalPlayers - 1)
            {
                screenDividers[i].SetActive(true);
            }
            else
            {
                screenDividers[i].SetActive(false);
            }
        }
    }

    public void UpdateCountdown(int num)
    {
        // show countdown
        if (num > 0)
        {
            countdownPanel.SetActive(true);
            countdownField.text = num.ToString();
        }
        else
        {
            countdownPanel.SetActive(false);
        }
    }
}
