﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityChooser : MonoBehaviour
{
    [SerializeField]
    private Image choosePrompt;
    [SerializeField]
    private Image startPrompt;

    [SerializeField]
    private Image[] abilities;

    private int currentIndex = 0;

    private void Start()
    {
        choosePrompt.enabled = true;
        startPrompt.enabled = false;
    }

    public void ResetChoosing()
    {
        choosePrompt.enabled = true;
        startPrompt.enabled = false;

        // reset animations / visuals
        for(int i = 0; i < abilities.Length; i++)
        {
            LeanTween.cancel(abilities[i].gameObject);
            abilities[i].transform.localScale = Vector3.one;
        }

        currentIndex = 0;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
        // TODO ability chooser hide effect
    }

    public void Show()
    {
        gameObject.SetActive(true);
        // TODO ability chooser show effect?
    }

    /// <summary>
    /// Sets the direction the player is choosing for the ability menu
    /// </summary>
    /// <param name="screenDirection">A vector where "up" is towards the top of the player's screen.
    ///     A very low or 0 magnitude vector means the indicator should be hidden.</param>
    public void SetAbilityChooseDirection(Vector2 screenDirection)
    {
        if (screenDirection.magnitude < PlayerController.DirectionDeadZone) return;

        int lastIndex = currentIndex;

        // ability choosing
        if(Mathf.Abs(screenDirection.x) > Mathf.Abs(screenDirection.y))
        {
            if (screenDirection.x > 0)
            {
                currentIndex = 2;
            }
            else
            {
                currentIndex = 4;
            }
        }
        else
        {
            if (screenDirection.y > 0)
            {
                currentIndex = 1;
            }
            else
            {
                currentIndex = 3;
            }
        }

        if(currentIndex != lastIndex)
        {
            if(lastIndex > 0)
            {
                // ability de-choose animation 
                LeanTween.cancel(abilities[lastIndex - 1].gameObject);
                LeanTween.scale(abilities[lastIndex - 1].gameObject, Vector3.one, 0.1f);
            }
            else
            {
                choosePrompt.enabled = false;
                startPrompt.enabled = true;
            }

            SoundManager.Instance.PlaySound(SoundName.ChooseNewAbility);

            // ability choose animation 
            LeanTween.cancel(abilities[currentIndex - 1].gameObject);
            abilities[currentIndex - 1].transform.localScale = Vector3.one;
            LeanTween.scale(abilities[currentIndex - 1].gameObject, Vector3.one * 1.25f, 0.1f);
        }
    }

    /// <summary>
    /// Returns the section the player has selected from the ability menu
    /// </summary>
    /// <returns>An index, where 0 = none, and 1-n = abilities starting at the top and going clockwise</returns>
    public int GetAbilityIndex()
    {
        return currentIndex;
    }

}
