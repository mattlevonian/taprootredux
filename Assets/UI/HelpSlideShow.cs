﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class HelpSlideShow : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;

    private int currentIndex;

    [SerializeField]
    private float waitTime;

    private float lastSwitch;

    private void Update()
    {
        if(Time.time - lastSwitch > waitTime)
        {
            lastSwitch = Time.time;
            currentIndex = (currentIndex + 1) % sprites.Length;
            GetComponent<Image>().sprite = sprites[currentIndex];
        }
    }

}
