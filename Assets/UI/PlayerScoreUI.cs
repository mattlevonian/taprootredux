﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This controls the score display in the corner.
/// </summary>
public class PlayerScoreUI : MonoBehaviour
{
    [SerializeField]
    private ScoreBar topBar;

    [SerializeField]
    private ScoreBar bottomBar;

    private Player myPlayer;

    public void Setup(Player p)
    {
        myPlayer = p;
        UpdateScores();
        GameManager.Instance.GlobalScoreKeeper.scoresChanged += UpdateScores;
    }

    public void UpdateScores()
    {
        // TODO different game modes (first to score vs timed)

        if(Mathf.Approximately(GameManager.Instance.GlobalScoreKeeper.TopScore, myPlayer.Score))
        {
            // our player is first
            if (Mathf.Approximately(myPlayer.Score, 0))
            {
                topBar.UpdateScore(0);
                topBar.SetColor(Color.gray);
                bottomBar.UpdateScore(0);
                bottomBar.SetColor(Color.gray);
            }
            else
            {
                topBar.UpdateScore(1);
                topBar.SetColor(myPlayer.playerColor);
                bottomBar.UpdateScore(GameManager.Instance.GlobalScoreKeeper.SecondScore / myPlayer.Score);
                bottomBar.SetColor(Player.Find(GameManager.Instance.GlobalScoreKeeper.SecondScoringPlayerIndex).playerColor);
            }
        }
        else
        {
            // our player is not first
            if (Mathf.Approximately(GameManager.Instance.GlobalScoreKeeper.TopScore, 0))
            {
                topBar.UpdateScore(0);
                topBar.SetColor(Color.gray);
                bottomBar.UpdateScore(0);
                bottomBar.SetColor(Color.gray);
            }
            else
            {
                topBar.UpdateScore(1);
                topBar.SetColor(Player.Find(GameManager.Instance.GlobalScoreKeeper.TopScoringPlayerIndex).playerColor);
                bottomBar.UpdateScore(myPlayer.Score / GameManager.Instance.GlobalScoreKeeper.TopScore);
                bottomBar.SetColor(myPlayer.playerColor);
            }
        }
    }
}
