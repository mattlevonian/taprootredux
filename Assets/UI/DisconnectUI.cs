﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisconnectUI : MonoBehaviour
{
    [SerializeField]
    private Text playerNameField;

    [SerializeField]
    private Image joinButtonImage;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void Init(Player p)
    {
        playerNameField.text = p.playerName;
        joinButtonImage.sprite = GameManager.Instance.GetButtonMapManager().GetSprite(p.DeviceClass, p.DeviceStyle, PlayerController.ConnectAction);
    }
}
