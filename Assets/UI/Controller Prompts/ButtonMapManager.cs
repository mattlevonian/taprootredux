﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

public class ButtonMapManager : MonoBehaviour
{
    [System.Serializable]
    private struct ButtonSet
    {
        public Sprite action1;
        public Sprite action2;
        public Sprite action3;
        public Sprite action4;

        public Sprite up;
        public Sprite down;
        public Sprite left;
        public Sprite right;

        public Sprite stick;
    }

    [SerializeField]
    private Sprite defaultSprite;

    [SerializeField]
    private ButtonSet xbox;
    [SerializeField]
    private ButtonSet ps4;
    [SerializeField]
    private ButtonSet keyboard;

    public Sprite GetSprite(InputDeviceClass deviceClass, InputDeviceStyle style, InputControlType control)
    {
        switch(deviceClass)
        {
            case InputDeviceClass.Controller:
                switch(style)
                {
                    case InputDeviceStyle.Xbox360:
                    case InputDeviceStyle.XboxOne:
                        return GetSpriteFromSet(xbox, control);
                    case InputDeviceStyle.PlayStation3:
                    case InputDeviceStyle.PlayStation4:
                        return GetSpriteFromSet(ps4, control);
                    default:
                        Debug.LogWarningFormat("No sprite found for style: {0} {1}", deviceClass, style, control);
                        return defaultSprite;
                }
            case InputDeviceClass.Unknown:
            case InputDeviceClass.Keyboard:
                return GetSpriteFromSet(keyboard, control);
            default:
                Debug.LogWarningFormat("No sprite found for device class: {0}", deviceClass);
                return defaultSprite;
        }
    }

    private Sprite GetSpriteFromSet(ButtonSet set, InputControlType control)
    {
        switch(control)
        {
            case InputControlType.Action1:
                return set.action1;

            case InputControlType.Action2:
                return set.action2;

            case InputControlType.Action3:
                return set.action3;

            case InputControlType.Action4:
                return set.action4;

            case InputControlType.LeftStickUp:
            case InputControlType.DPadUp:
                return set.up;

            case InputControlType.LeftStickDown:
            case InputControlType.DPadDown:
                return set.down;

            case InputControlType.LeftStickLeft:
            case InputControlType.DPadLeft:
                return set.left;

            case InputControlType.LeftStickRight:
            case InputControlType.DPadRight:
                return set.right;

            case InputControlType.LeftStickButton:
                return set.stick;

            default:
                Debug.LogWarningFormat("No sprite found for control type on device: {0}", control);
                return defaultSprite;
        }
    }
}
