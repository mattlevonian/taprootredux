﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

[RequireComponent(typeof(Image))]
public class LoadWithButton : MonoBehaviour
{
    [SerializeField]
    private InputDeviceClass deviceClass;
    [SerializeField]
    private InputDeviceStyle deviceStyle;
    [SerializeField]
    private InputControlType controlType;

    private void Start ()
    {
        GetComponent<Image>().sprite = GameManager.Instance.GetButtonMapManager().GetSprite(deviceClass, deviceStyle, controlType);
	}
}
