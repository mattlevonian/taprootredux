﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

[RequireComponent(typeof(Image))]
public class PlayerControlImage : MonoBehaviour
{
    [SerializeField]
    private InputControlType controlType;

    public void Setup(Player player)
    {
        GetComponent<Image>().sprite = GameManager.Instance.GetButtonMapManager().GetSprite(player.DeviceClass, player.DeviceStyle, controlType);
    }
}
