﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

[RequireComponent(typeof(Image))]
public class ActiveControlButton : MonoBehaviour
{
    [SerializeField]
    private InputControlType controlType;

    private void Start ()
    {
        InputManager.OnActiveDeviceChanged += InputManager_OnActiveDeviceChanged;
        InputManager_OnActiveDeviceChanged(InputManager.ActiveDevice);
    }

    private void OnDestroy()
    {
        InputManager.OnActiveDeviceChanged -= InputManager_OnActiveDeviceChanged;
    }

    private void InputManager_OnActiveDeviceChanged(InputDevice obj)
    {
        GetComponent<Image>().sprite = GameManager.Instance.GetButtonMapManager().GetSprite(obj.DeviceClass, obj.DeviceStyle, controlType);
    }
}
