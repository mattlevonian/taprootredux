﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    // TODO minimap annotations

    [SerializeField]
    private RawImage image;

    private void OnEnable()
    {
        var size = image.GetComponent<RectTransform>().sizeDelta;
        size.y = size.x;
        image.GetComponent<RectTransform>().sizeDelta = size;
    }
}
