﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

/// <summary>
/// Starts attached to the persistent GameManager object. Responsible for tracking all
/// local attached InputDevices and their PlayerControllers.
/// </summary>
public class PlayerControllerManager : MonoBehaviour
{
    /// <summary>
    /// Maps each attached device to its PlayerController, even for unused devices
    /// </summary>
    private Dictionary<InputDevice, PlayerController> controllers;

    /// <summary>
    /// A list of controllers where the player has pressed a button to "activate" it for use.
    /// </summary>
    private List<InputDevice> activatedDevices;

    public List<PlayerController> ActivatedPlayerControllers
    {
        get
        {
            List<PlayerController> list = new List<PlayerController>();
            foreach(InputDevice d in activatedDevices)
            {
                list.Add(controllers[d]);
            }
            return list;
        }
    }

    private PlayerController lastControllerUsed;
    public PlayerController LastControllerUsed
    {
        get { return lastControllerUsed; }
    }

    public event Action onControllerSetupChanged;

    [SerializeField]
    private ActivePlayerUI playerUI;

    private void Start()
    {
        controllers = new Dictionary<InputDevice, PlayerController>();
        activatedDevices = new List<InputDevice>();

        InControl.InputManager.OnDeviceAttached += OnDeviceAttached;
        InControl.InputManager.OnDeviceDetached += OnDeviceDetached;

        // auto-add keyboard as special device case
        PlayerController_Keyboard k = new PlayerController_Keyboard();
        controllers.Add(k.Device, k);
        k.onControllerUsed += OnControllerUsed;

        // init list of devices with pre-existing devices
        Debug.Log("Setting up devices that are already attached:");
        foreach (InputDevice device in InControl.InputManager.Devices)
        {
            Debug.Log(device.Name);

            PlayerController c = new PlayerController(device);
            controllers.Add(device, c);
            c.onControllerUsed += OnControllerUsed;
        }

        playerUI.UpdateDevicesUI(controllers, activatedDevices);
    }
    private void OnDeviceAttached(InputDevice device)
    {
        if(controllers.ContainsKey(device))
        {
            Debug.LogWarningFormat("A device just got added more than once ({0})", device.Name);
            return;
        }

        Debug.LogFormat("Device attached: {0}", device.Name);

        PlayerController c = new PlayerController(device);
        controllers.Add(device, c);
        c.onControllerUsed += OnControllerUsed;

        playerUI.UpdateDevicesUI(controllers, activatedDevices);
    }
    private void OnDeviceDetached(InputDevice device)
    {
        if (!controllers.ContainsKey(device))
        {
            Debug.LogWarningFormat("A device just got removed more than once ({0})", device.Name);
            return;
        }

        Debug.LogFormat("Device detached: {0}", device.Name);

        controllers[device].DetachFromPlayer();
        controllers[device].onControllerUsed -= OnControllerUsed;
        controllers.Remove(device);
        activatedDevices.Remove(device);

        playerUI.UpdateDevicesUI(controllers, activatedDevices);
    }

    private void OnControllerUsed(InputDevice device)
    {
        if (!activatedDevices.Contains(device))
        {
            activatedDevices.Add(device);

            if (onControllerSetupChanged != null)
            {
                onControllerSetupChanged.Invoke();
            }

            playerUI.UpdateDevicesUI(controllers, activatedDevices);
        }

        lastControllerUsed = controllers[device];
    }
    public void OnControllerUnused(InputDevice device)
    {
        if (activatedDevices.Contains(device))
        {
            activatedDevices.Remove(device);

            if (onControllerSetupChanged != null)
            {
                onControllerSetupChanged.Invoke();
            }

            playerUI.UpdateDevicesUI(controllers, activatedDevices);
        }
    }

    private void Update()
    {
        foreach(PlayerController controller in controllers.Values)
        {
            controller.Update();
        }
    }

    #region UI
    #endregion
}
