﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

/// <summary>
/// The GameManager exists in the starting scene and 
/// the first instance persists throughout the session.
/// 
/// It handles networking states and events, 
/// manages connected player controllers, 
/// references to the scorekeeper, pausing, 
/// game countdowns, UI switching, including spawning player-specific UI (view contexts)
/// </summary>
public class GameManager : MonoBehaviour
{
    public const int MinimapLayer = 10;

    private const bool PauseMenuPausesGame = false;
    private const int PreGameCountdownLength = 2;

    [SerializeField]
    private int TutorialMapBuildIndex;
    [SerializeField]
    private TutorialController tutorialControllerPrefab;

    public const bool AllowChoppingOwnRoots = false;

    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private PlayerControllerManager playerControllerManager;
    public PlayerControllerManager GetPlayerControllerManager()
    {
        return playerControllerManager;
    }

    private ButtonMapManager buttonMapManager;
    public ButtonMapManager GetButtonMapManager()
    {
        return buttonMapManager;
    }

    [SerializeField]
    private string ScoreKeeperPrefabName;
    internal ScoreKeeper GlobalScoreKeeper;

    private enum Phase
    {
        NotInGame,
        InLobby,
        InGame,
        InPostGame,
        InTutorial
    }
    private Phase currentPhase;

    public bool InLobby
    {
        get
        {
            return currentPhase == Phase.InLobby;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;

            DontDestroyOnLoad(this.gameObject);

            playerControllerManager = GetComponent<PlayerControllerManager>();
            buttonMapManager = GetComponent<ButtonMapManager>();

            SceneManager.sceneLoaded += OnLevelFinishedLoading;
            GetPlayerControllerManager().onControllerSetupChanged += CheckCanHost;

            StartNetworking();

            SetPhase(Phase.NotInGame);
            joinUI.SetHostCallback(HostRoom, HostTutorial);

            pauseMenuUI.gameObject.SetActive(false);
            pausedUI.SetActive(false);

        }
        else
        {
            Debug.LogWarning("There are multiple Network Managers. New one is self-destructing");

            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        if (currentPhase == Phase.InGame && currentCountdown > 0)
        {
            if (Time.time - countdownStartTime > 1)
            {
                countdownStartTime = Time.time;
                currentCountdown -= 1;

                SendEvent(EventCode_Countdown, currentCountdown);
            }
        }
        else if (currentPhase == Phase.InGame && GameTimeLeft < 5)
        {
            if(GameTimeLeft <= 0)
            {
                EndGame();
            }
        }
    }

    #region Pausing and Pause Menu

    private GameObject lastFocusedObject;
    private bool pauseMenuVisible;

    [SerializeField]
    private PauseUI pauseMenuUI;

    [SerializeField]
    private GameObject pausedUI;

    public void TogglePauseMenu(PlayerController pausingController)
    {
        if (pauseMenuVisible)
        {
            Debug.Log("hiding pause menu");

            pauseMenuVisible = false;
            pauseMenuUI.gameObject.SetActive(false);
            EventSystem.current.SetSelectedGameObject(lastFocusedObject);

            if (PhotonNetwork.isMasterClient && PauseMenuPausesGame)
            {
                SendEvent(EventCode_PauseGame, false);
            }
        }
        else
        {
            //if(currentPhase == Phase.InGame)
            {
                Debug.Log("showing pause menu");

                pauseMenuVisible = true;
                pauseMenuUI.gameObject.SetActive(true);
                lastFocusedObject = EventSystem.current.currentSelectedGameObject;
                pauseMenuUI.SetController(pausingController);
                pauseMenuUI.Focus();

                if (PhotonNetwork.isMasterClient && PauseMenuPausesGame && currentPhase == Phase.InGame)
                {
                    SendEvent(EventCode_PauseGame, true);
                }
            }
        }
    }

    // called by a network event on every client; only the master client can pause
    public void SetPaused(bool paused)
    {
        if (paused)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }

        if(!PhotonNetwork.isMasterClient)
        {
            pausedUI.SetActive(paused);
        }
    }

    public void MapUp()
    {
        lobbyUI.MapUp();
    }

    public void MapDown()
    {
        lobbyUI.MapDown();
    }

    #endregion

    #region Player Management

    [SerializeField]
    private ViewContext viewContextPrefab;

    private void CheckCanHost()
    {
        joinUI.SetCanHost(GetPlayerControllerManager().ActivatedPlayerControllers.Count > 0);
    }

    private List<Player> disconnectedPlayers;

    public void TryAttachToPlayer(PlayerController c)
    {
        if(disconnectedPlayers != null && disconnectedPlayers.Count > 0)
        { 
            Player p = disconnectedPlayers[0];
            disconnectedPlayers.RemoveAt(0);
            c.AttachToPlayer(p);

            ShowPlayerDisconnectedUI(null);
        }

        if (disconnectedPlayers != null && disconnectedPlayers.Count > 0)
        {
            ShowPlayerDisconnectedUI(disconnectedPlayers[0]);
        }

        // TODO allow new players to be created in the lobby if there is space
    }
    public void PlayerIsDisconnected(Player p)
    {
        disconnectedPlayers.Add(p);

        if(disconnectedPlayers.Count == 1)
        {
            ShowPlayerDisconnectedUI(disconnectedPlayers[0]);
        }
    }

    [SerializeField]
    private DisconnectUI controllerDisconnectUI;

    public void ShowPlayerDisconnectedUI(Player p)
    {
        if (p != null)
        {
            controllerDisconnectUI.gameObject.SetActive(true);
            controllerDisconnectUI.Init(p);
        }
        else
        {
            controllerDisconnectUI.gameObject.SetActive(false);
        }
    }

    public void UpdatePlayerList()
    {
        lobbyUI.UpdatePlayerList();
        CheckReadyToStart();
    }

    #endregion

    #region Networking

    public const string GameVersion = "0.0.1";


    // event codes
    public const byte EventCode_StartGame = 1;
    public const byte EventCode_EndGame = 2;
    public const byte EventCode_ReturnToLobby = 3;
    public const byte EventCode_PauseGame = 4;
    public const byte EventCode_Countdown = 5;
    public const byte EventCode_StartTutorial = 6;

    private float countdownStartTime;
    private int currentCountdown = 5;

    [SerializeField]
    private float gameLength;
    private float gameStartTime;

    // false when in lobby, pre-game countdown, or post-game scorescreen
    private bool playingGame;

    public float GameTimeLeft
    {
        get
        {
            if(currentCountdown > 0 && !playingGame)
            {
                return gameLength;
            }
            return Mathf.Max(0, gameLength - (Time.time - gameStartTime));
        }
    }

    [SerializeField]
    private string playerPrefabName;

    [SerializeField]
    private GameJoinUI joinUI;

    [SerializeField]
    private ActivePlayerUI deviceUI;

    [SerializeField]
    private RoomLobbyUI lobbyUI;

    [SerializeField]
    private InGameUI ingameUI;

    [SerializeField]
    private PostGameUI postGameUI;

    public void CheckReadyToStart()
    {
        if (PhotonNetwork.isMasterClient)
        {
            lobbyUI.HideStartButton(false);

            foreach (Player p in Player.Players)
            {
                if (p != null && !p.isReady)
                {
                    lobbyUI.SetAbleToStart(false);
                    return;
                }
            }

            lobbyUI.SetAbleToStart(true);
        }
        else
        {
            lobbyUI.HideStartButton(true);
        }
    }

    private string currentRoomName;
    public string GetRoomName()
    {
        return currentRoomName;
    }

    private void StartNetworking()
    {
        PhotonNetwork.OnEventCall += OnRaiseEvent;
        PhotonNetwork.ConnectUsingSettings(GameVersion);
    }

    private void SendEvent(byte eventCode, object content = null)
    {
        Debug.Log("Sending event");
        RaiseEventOptions options = new RaiseEventOptions();
        options.Receivers = ReceiverGroup.All;
        if (PhotonNetwork.offlineMode)
        {
            OnRaiseEvent(eventCode, content, PhotonNetwork.player.ID);
        }
        else
        {
            PhotonNetwork.RaiseEvent(eventCode, content, true, options);
        }
    }
    private void OnRaiseEvent(byte eventCode, object content, int senderId)
    {
        Debug.Log("Recieved event");
        if (_instance != this) return;
        switch (eventCode)
        {
            case EventCode_StartTutorial:
                Debug.Log("Recieved StartTutorial network event");
                LoadScene((int)content, true);
                SetPhase(Phase.InTutorial);
                break;
            case EventCode_StartGame:
                Debug.Log("Recieved StartGame network event");
                LoadScene((int)content, true);
                SetPhase(Phase.InGame);
                break;
            case EventCode_EndGame:
                Debug.Log("Recieved EndGame network event");
                LoadScene(0);
                SetPhase(Phase.InPostGame);
                break;
            case EventCode_ReturnToLobby:
                Debug.Log("Recieved ReturnToLobby network event");

                foreach(Player p in Player.LocalPlayers)
                {
                    p.SetReady(false);
                }

                LoadScene(0);
                SetPhase(Phase.InLobby);
                break;
            case EventCode_PauseGame:
                Debug.Log("Recieved PauseGame network event");
                SetPaused((bool)content);
                break;
            case EventCode_Countdown:
                Debug.Log("Recieved Countdown network event");
                if (currentPhase == Phase.InGame)
                {
                    ingameUI.UpdateCountdown((int)content);

                    if((int)content == 0)
                    {
                        gameStartTime = Time.time;
                        playingGame = true;

                        foreach (Player p in Player.LocalPlayers)
                        {
                            p.StartPlay();
                        }
                    }
                }
                break;
            default:
                Debug.LogWarning("Recieved unknown network event!");
                break;
        }
    }

    private bool tutorialMode;

    /// <summary>
    /// Causes a new Photon room to be created and enters into a tutorial game
    /// </summary>
    public void HostTutorial()
    {
        tutorialMode = true;

        PhotonNetwork.offlineMode = true;
        joinUI.SetOnlinePlayEnabled(false);

        bool result = PhotonNetwork.CreateRoom("Tutorial Room");

        if (!result)
        {
            Debug.LogWarning("HostTutorial failed.");
        }
    }

    // used to prevent multiple join/host events from UI
    // true if we have submitted a host or join request and it hasn't returned yet;
    private bool networkAttemptInProgress;

    /// <summary>
    /// Causes a new Photon room to be created.
    /// </summary>
    public void HostRoom(bool online)
    {
        // avoid multiple calls of this from repeated button presses
        if (!networkAttemptInProgress)
        {
            networkAttemptInProgress = true;

            if (online)
            {
                string[] part1 = { "Random", "Awesome", "Amazing", "Glorious", "Epic", "Legendary", "Fantastic" };
                string[] part2 = { "Shindig", "Party", "Event", "Conference", "Convention", "Throwdown", "Adventure" };

                string roomName = part1[UnityEngine.Random.Range(0, part1.Length)] + " " + part2[UnityEngine.Random.Range(0, part2.Length)];
                currentRoomName = roomName;

                Debug.Log("Hosting a room called " + roomName);

                bool result = PhotonNetwork.CreateRoom(roomName);

                if (!result)
                {
                    Debug.LogWarning("Online HostRoom failed.");
                    networkAttemptInProgress = false;
                }
            }
            else
            {
                PhotonNetwork.offlineMode = true;
                joinUI.SetOnlinePlayEnabled(false);

                bool result = PhotonNetwork.CreateRoom("Offline Room");

                if (!result)
                {
                    Debug.LogWarning("Offline HostRoom failed.");
                    networkAttemptInProgress = false;
                }
            }
        }
    }
    /// <summary>
    /// Causes the client to connect to an existing Photon room
    /// </summary>
    /// <param name="roomName">The name of the room</param>
    private void JoinRoom(string roomName)
    {
        // avoid multiple calls of this from repeated button presses
        if (!networkAttemptInProgress)
        {
            networkAttemptInProgress = true;

            Debug.Log("Trying to join a room called " + roomName);

            currentRoomName = roomName;

            bool result = PhotonNetwork.JoinRoom(roomName);

            if (!result)
            {
                Debug.LogError("JoinRoom failed");
                networkAttemptInProgress = false;
            }
        }
    }
    private void OnJoinedRoom()
    {
        networkAttemptInProgress = false;
        Debug.Log("Successfully joined a room called " + currentRoomName);

        if (tutorialMode)
        {
            PlayerController c = GetComponent<PlayerControllerManager>().LastControllerUsed;
            Player p = PhotonNetwork.Instantiate(playerPrefabName, Vector3.zero, Quaternion.identity, 0).GetComponent<Player>();
            c.AttachToPlayer(p);

            TutorialController tc = Instantiate(tutorialControllerPrefab);
            tc.Init();
            tc.tutorialCompleted += EndGame;

            SendEvent(EventCode_StartTutorial, TutorialMapBuildIndex);
        }
        else
        {
            disconnectedPlayers = new List<Player>();

            // TODO handle the case when there are too many players, both locally and remotely
            foreach (PlayerController c in GetComponent<PlayerControllerManager>().ActivatedPlayerControllers)
            {
                Player p = PhotonNetwork.Instantiate(playerPrefabName, Vector3.zero, Quaternion.identity, 0).GetComponent<Player>();
                c.AttachToPlayer(p);
            }

            if (PhotonNetwork.isMasterClient)
            {
                PhotonNetwork.Instantiate(ScoreKeeperPrefabName, Vector3.zero, Quaternion.identity, 0);
            }

            SetPhase(Phase.InLobby);

            lobbyUI.UpdatePlayerList();
            CheckReadyToStart();
        }
    }

    public void StartGame()
    {
        if (PhotonNetwork.isMasterClient && !playingGame)
        {
            PhotonNetwork.room.IsOpen = false;
            PhotonNetwork.room.IsVisible = false;

            SendEvent(EventCode_StartGame, lobbyUI.CurrentMapBuildIndex);
        }
        else
        {
            Debug.Log("StartGame() called, but client is not MasterClient, or game is already playing.");
        }
    }

    public void EndGame()
    {
        if (PhotonNetwork.isMasterClient && playingGame)
        {
            byte eventCode = tutorialMode ? EventCode_ReturnToLobby : EventCode_EndGame;

            playingGame = false;
            tutorialMode = false;

            SendEvent(eventCode);
        }
    }

    public void ReturnToLobby()
    {
        if (PhotonNetwork.isMasterClient)
        {
            PhotonNetwork.room.IsOpen = true;
            PhotonNetwork.room.IsVisible = true;

            SendEvent(EventCode_ReturnToLobby);

            GlobalScoreKeeper.ResetScores();
        }
    }

    public void LeaveRoom()
    {
        Debug.Log("Disconnecting from the room.");

        PhotonNetwork.LeaveRoom();
        SetPaused(false);
    }
    private void OnLeftRoom()
    {
        Debug.Log("Disconnected from room.");

        playingGame = false;
        tutorialMode = false;

        // load back to main menu
        LoadScene(0);
        SetPhase(Phase.NotInGame);

        currentRoomName = null;

        if (PhotonNetwork.offlineMode)
        {
            // re-enable networking in case we came from an offline room
            PhotonNetwork.offlineMode = false;
            PhotonNetwork.ConnectUsingSettings(GameVersion);
        }

        Branch.ResetGUID();
    }

    // Initial connection stack
    private void OnConnectedToMaster()
    {
        Debug.Log("Connected to master game server... Attempting to join lobby and get rooms");
        PhotonNetwork.JoinLobby();
    }
    private void OnJoinedLobby()
    {
        Debug.Log("Connected to lobby! Listing open rooms now...");
        GetRoomList();
    }
    private void OnReceivedRoomListUpdate()
    {
        Debug.Log("Recieved room list update");
        GetRoomList();
    }
    private void GetRoomList()
    {
        //Get new room info from server
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();

        Debug.Log("Found info for " + rooms.Length + " rooms.");

        joinUI.SetOnlinePlayEnabled(true);
        joinUI.UpdateListOfRooms(rooms, JoinRoom);
    }

    // called before OnPhotonPlayerDisconnected
    private void OnMasterClientSwitched()
    {
        Debug.Log("OnMasterClientSwitched()");
        Debug.Log("isMasterClient " + PhotonNetwork.isMasterClient);
    }

    private void OnPhotonPlayerConnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerConnected() " + other.NickName);
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer other)
    {
        Debug.Log("OnPhotonPlayerDisconnected() " + other.NickName);
    }

    #endregion

    private void LoadScene(int index, bool additive = false)
    {
        PhotonNetwork.isMessageQueueRunning = false;
        SceneManager.LoadSceneAsync(index, additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        SceneManager.SetActiveScene(scene);
        PhotonNetwork.isMessageQueueRunning = true;

        if (scene.buildIndex > 0)
        {
            // set up player view contexts
            int numLocalPlayers = Player.LocalPlayers.Count;
            for (int i = 0; i < numLocalPlayers; i++)
            {
                Player p = Player.LocalPlayers[i];
                p.viewContext = Instantiate(viewContextPrefab);
                p.viewContext.Setup(p, numLocalPlayers, i);
            }
            foreach (Player p in Player.Players)
            {
                if(p != null)
                {
                    p.Setup();
                }
            }
        }
    }

    private void SetPhase(Phase newPhase)
    {
        switch(newPhase)
        {
            case Phase.NotInGame:

                deviceUI.gameObject.SetActive(true);
                joinUI.gameObject.SetActive(true);
                lobbyUI.gameObject.SetActive(false);
                ingameUI.gameObject.SetActive(false);
                postGameUI.gameObject.SetActive(false);

                joinUI.Focus();

                break;

            case Phase.InLobby:

                deviceUI.gameObject.SetActive(false);
                joinUI.gameObject.SetActive(false);
                lobbyUI.gameObject.SetActive(true);
                ingameUI.gameObject.SetActive(false);
                postGameUI.gameObject.SetActive(false);

                lobbyUI.Focus();

                break;

            case Phase.InGame:

                deviceUI.gameObject.SetActive(false);
                joinUI.gameObject.SetActive(false);
                lobbyUI.gameObject.SetActive(false);
                ingameUI.gameObject.SetActive(true);
                postGameUI.gameObject.SetActive(false);

                currentCountdown = 3;

                ingameUI.Init(Player.LocalPlayers.Count);

                countdownStartTime = Time.time;
                currentCountdown = PreGameCountdownLength;
                ingameUI.UpdateCountdown(currentCountdown);

                break;

            case Phase.InPostGame:

                deviceUI.gameObject.SetActive(false);
                joinUI.gameObject.SetActive(false);
                lobbyUI.gameObject.SetActive(false);
                ingameUI.gameObject.SetActive(false);
                postGameUI.gameObject.SetActive(true);

                postGameUI.UpdatePlayerList();

                postGameUI.Focus();

                playingGame = false;

                foreach (Player p in Player.Players)
                {
                    if(p != null)
                    {
                        p.StopPlay();
                    }
                }

                SetPaused(false);

                break;

            case Phase.InTutorial:

                deviceUI.gameObject.SetActive(false);
                joinUI.gameObject.SetActive(false);
                lobbyUI.gameObject.SetActive(false);
                ingameUI.gameObject.SetActive(true);
                postGameUI.gameObject.SetActive(false);


                ingameUI.Init(1);

                countdownStartTime = Time.time;
                currentCountdown = 0;
                ingameUI.UpdateCountdown(0);

                playingGame = true;

                break;
        }

        currentPhase = newPhase;
    }
}
