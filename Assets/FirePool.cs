﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePool : Pool
{
    /// <summary>
    /// Called when a branch starts being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the new branch</param>
    public override void AddBranch(Branch b)
    {
        base.AddBranch(b);

        if(b.myRoot.player.isLocal)
        {
            Debug.Log("Setting a branch on fire");
            b.myRoot.player.CatchFire(b.myRoot.rootIndex, b.myTipIndex, b.SerializableGrowthValue);
        }
    }

    /// <summary>
    /// Called when a branch stops being connected to this pool
    /// </summary>
    /// <param name="b">A reference to the branch being disconnected</param>
    public override void RemoveBranch(Branch b)
    {
        base.RemoveBranch(b);
    }
}
