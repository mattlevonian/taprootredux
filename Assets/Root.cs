﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used as a handle or wrapper to control an entire root system.
/// </summary>
public class Root : MonoBehaviour, IPunObservable
{
    /// <summary>
    /// A reference to the controllable, growing tip of the root.
    /// </summary>
    private Branch activeTip;

    internal int ActiveTipIndex
    {
        get
        {
            return activeTip.myTipIndex;
        }
    }

    // whether or not the root is in the focus of its player (i.e. being controlled or looked at)
    internal bool isFocused;

    // the bounds enclosing all of the root's tips, used to show the entire root on-screen
    internal Bounds worldRect;

    [SerializeField, Tooltip("A prefab for the branch at the base of the root")]
    private Branch startingBranchPrefab;
    private Branch startingBranch;

    internal int rootIndex;
    internal Player player;

    [SerializeField, Tooltip("This many points will be subtracted if a fire burns to the base")]
    private float pointsLostFromFireDeath;

    [SerializeField, Tooltip("After the root dies, how long does it persist")]
    private float timeToDestroy;

    internal bool hasStarted
    {
        get;
        private set;
    }

    private void Awake()
    {
        growingBranches = new List<Branch>();
        hasStarted = false;
    }

    private void Start()
    {
        // always create a starting branch immediately
        startingBranch = Instantiate(startingBranchPrefab, this.transform, false);
        startingBranch.worldGrowDirection = this.transform.up;
        startingBranch.myRoot = this;
        activeTip = startingBranch;
        growingBranches.Add(activeTip);
        activeTip.myTipIndex = 0;

        worldRect = new Bounds(transform.position, Vector3.zero);
    }

    private void Update()
    {
        foreach (Branch tip in growingBranches)
        {
            worldRect.Encapsulate(tip.EndPositionInWorld);
        }
    }

    public int AbilityIndex { get; private set; }

    public void SetAbility(int index)
    {
        AbilityIndex = index;

        // ability effects
        switch (index)
        {
            case 1: // fast grows
                startingBranch.EnableFastGrowth();
                break;

            case 2: // fast drain
                startingBranch.EnableFastDrain();
                break;

            case 3: // cutting
                startingBranch.EnableRootCutting();
                break;

            case 4: // defense
                startingBranch.EnableArmor();
                break;
        }
    }

    public void StartGrowing()
    {
        hasStarted = true;
        startingBranch.StartGrowing();
    }

    public void DisableCutting()
    {
        AbilityIndex = 0;
        foreach(Branch b in growingBranches)
        {
            b.DisableCutting();
        }
    }

    public bool ActiveTipIsGrowing
    {
        get
        {
            if(activeTip == null) return false;
            return activeTip.isGrowing;
        }
    }

    /// <summary>
    /// Calls down to the active tip and returns whether or not the branch can currently split
    /// </summary>
    /// <returns>Whether or not the tip is allowed to split</returns>
    public bool AllowedToBranch(Vector3 worldDirection)
    {
        return activeTip.AllowedToBranch(worldDirection);
    }

    /// <summary>
    /// Calls down to the active tip and causes it to branch. 
    /// Also aligns the branch state with the owning client before doing so.
    /// </summary>
    /// <param name="branchStart">The point in space where the owning client's tip was when they created the branch.</param>
    /// <param name="direction">The new direction, in world space, that the active tip should grow in</param>
    public void CreateBranchInWorldDirection(Vector3 branchStart, Vector3 direction)
    {
        activeTip.myTipIndex = -1;

        activeTip.ForceEndToWorldPosition(branchStart);
        activeTip.CreateBranchInWorldDirection(direction);
        activeTip = activeTip.secondaryChild;

        growingBranches[growingBranches.Count - 1] = activeTip.parent.primaryChild;
        activeTip.parent.primaryChild.myTipIndex = growingBranches.Count - 1;

        growingBranches.Add(activeTip);
        activeTip.myTipIndex = growingBranches.Count - 1;

        if (player.isLocal || activeTip.CanBeSeenByLocalPlayer())
        {
            SoundManager.Instance.PlaySound(SoundName.NewBranch);
        }
    }

    /// <summary>
    /// Calls down to the active tip and gets its position in world space
    /// </summary>
    /// <returns>The position of the active tip in world space</returns>
    public Vector3 GetActiveTipPosition()
    {
        if(activeTip == null)
        {
            Debug.LogWarning("A root didn't have an active tip (OK for a frame)");
            return Vector3.zero;
        }
        return activeTip.EndPositionInWorld;
    }

    /// <summary>
    /// Calls down to the given tip and gets its position in world space
    /// </summary>
    /// <param name="tipIndex">The index of the growing tip being referenced</param>
    /// <returns>The position of the corresponding tip in world space</returns>
    public Vector3 GetBranchTipEnd(int tipIndex)
    {
        return growingBranches[tipIndex].EndPositionInWorld;
    }

    public void ForceDestroy()
    {
        PhotonNetwork.Destroy(this.gameObject);
    }

    public void AddResources(float amount)
    {
        player.AddResources(amount);
    }

    /// <summary>
    /// Cuts the branch. Causes it to die shortly thereafter, and prevents any more nutrients from being soaked up
    /// </summary>
    /// <param name="tipEnd">The point in space where the owning client's tip was when they cut the branch.</param>
    public void Cut(Vector3 tipEnd)
    {
        activeTip.ForceEndToWorldPosition(tipEnd);
        startingBranch.Cut();
        StartCoroutine(DestroyRoutine());
    }

    // this routine blackens and fades the root, etc.
    private IEnumerator DestroyRoutine()
    {
        float time = 0;
        while(time < timeToDestroy)
        {
            startingBranch.SetWidthModifier(1 - (time / timeToDestroy));
            time += Time.deltaTime;
            yield return null;
        }
        startingBranch.RecursiveDestroy();
        Destroy(this.gameObject);
    }

    public void TipCatchFire(int tipIndex, float growthAtTime)
    {
        growingBranches[tipIndex].ForceEndToGrowthValue(growthAtTime);
        growingBranches[tipIndex].CatchFire(1, true);
    }

    public void FireReachedBase()
    {
        player.CutBranch(rootIndex);

        // remove points
        player.AddResources(-pointsLostFromFireDeath);
    }

    public void CollidedWithObject(int tipIndex, Vector3 tipEnd, GameObject obj)
    {
        growingBranches[tipIndex].ForceEndToWorldPosition(tipEnd);
        growingBranches[tipIndex].OnTipImpact(obj);
    }

    // A list of all the active branches is maintained and updated every time a split occurs.
    // N
    private List<Branch> growingBranches;
    internal List<Branch> GrowingBranches
    {
        get
        {
            return growingBranches;
        }
    }

    // Active branches are synced by sending the "growth value" of each one. 
    // Growth value is essentially number of units grown from the start.
    private float[] growthArray;
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            if(growthArray == null || growthArray.Length < growingBranches.Count)
            {
                growthArray = new float[growingBranches.Count];
            }

            for(int i = 0; i < growthArray.Length; i++)
            {
                growthArray[i] = growingBranches[i].SerializableGrowthValue;
            }

            stream.SendNext(growthArray);
        }
        else
        {
            growthArray = (float[]) stream.ReceiveNext();

            for (int i = 0; i < growthArray.Length; i++)
            {
                if (i < growingBranches.Count)
                {
                    growingBranches[i].ForceEndToGrowthValue(growthArray[i]);
                }
            }
        }
    }
}
