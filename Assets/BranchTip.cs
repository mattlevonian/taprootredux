﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Used to recieve collisions from the physics system
/// </summary>
public class BranchTip : MonoBehaviour
{
    internal Branch myBranch;

    private Rigidbody2D myRigidbody;

    [SerializeField]
    private CircleCollider2D circleCollder;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        myRigidbody.position = myBranch.EndPositionInWorld;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        // we can't collide with ourselves
        if (!myBranch.IsSelfOrSiblingOrParent(collision.collider.gameObject))
        {
            myBranch.OnTipImpact(collision.collider.gameObject);
        }
    }

    public void SetRadius(float tipRadius)
    {
        circleCollder.radius = tipRadius;
    }
}
