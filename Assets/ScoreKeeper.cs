﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreKeeper : MonoBehaviour, IPunObservable
{
    private void Awake()
    {
        DontDestroyOnLoad(this);

        GameManager.Instance.GlobalScoreKeeper = this;
    }

    public event Action scoresChanged;

    // TODO deal with scores of players leaving mid-game

    private float[] scores = new float[Player.MaxNumPlayers];

    public float TopScore { get; private set; }
    public int TopScoringPlayerIndex { get; private set; } // not guaranteed to be unique

    public float SecondScore { get; private set; }
    public int SecondScoringPlayerIndex { get; private set; } // not guaranteed to be unique

    public void ResetScores()
    {
        for(int i = 0; i < scores.Length; i++)
        {
            scores[i] = 0;
        }
    }

    public void AddToScore(int playerIndex, float amount)
    {
        scores[playerIndex] += amount;

        for (int i = 0; i < scores.Length; i++)
        {
            if (scores[i] > TopScore)
            {
                TopScore = scores[i];
                TopScoringPlayerIndex = i;
            }
        }
        for (int i = 0; i < scores.Length; i++)
        {
            if (scores[i] > SecondScore && scores[i] < TopScore)
            {
                SecondScore = scores[i];
                SecondScoringPlayerIndex = i;
            }
        }

        if(scoresChanged != null)
        {
            scoresChanged.Invoke();
        }
    }

    public float GetScoreByPlayerIndex(int playerIndex)
    {
        return scores[playerIndex];
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(scores);
        }
        else
        {
            scores = (float[])stream.ReceiveNext();
        }
    }
}
