﻿Shader "Unlit/UnlitBlend"
{
	Properties
	{
		_FirstTex("First Texture", 2D) = "white" {}
		_SecondTex("Second Texture", 2D) = "white" {}
		_Blend ("Blend Factor", float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};

			sampler2D _SecondTex;
			float4 _SecondTex_ST;

			sampler2D _FirstTex;
			float4 _FirstTex_ST;

			float _Blend;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _FirstTex);
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col1 = tex2D(_FirstTex, i.uv);
				fixed4 col2 = tex2D(_SecondTex, i.uv);
				fixed4 col = col2 * _Blend + col1 * (1 - _Blend);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				return col;
			}
			ENDCG
		}
	}
}
