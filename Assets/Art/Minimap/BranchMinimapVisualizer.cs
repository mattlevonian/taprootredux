﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Branch))]
public class BranchMinimapVisualizer : MonoBehaviour
{
    private Branch myBranch;

    [SerializeField]
    private GameObject greybox;

    [SerializeField]
    private float width;

    private float widthModifier = 1;

    private bool colorInitialized;

    private void Start()
    {
        myBranch = GetComponent<Branch>();

        greybox.transform.localScale = new Vector3(width * widthModifier, myBranch.tipRadius, 1);
        greybox.transform.localPosition = new Vector3(0, myBranch.tipRadius / 2, 0);

        if (myBranch.myRoot.player != null)
        {
            colorInitialized = true;
            greybox.GetComponent<Renderer>().material.color = myBranch.myRoot.player.playerColor;
        }

        myBranch.newBurnEventAdded += BurnEventAdded;
    }

    private List<Branch.BurnEvent> burnEvents = new List<Branch.BurnEvent>();
    private List<GameObject> burnBoxes = new List<GameObject>();

    private void BurnEventAdded(Branch.BurnEvent be)
    {
        burnEvents.Add(be);
        burnBoxes.Add(Instantiate(greybox, greybox.transform.parent, false));
        burnBoxes[burnBoxes.Count - 1].GetComponent<Renderer>().material.color = Color.red;
    }

    private void Update()
    {
        float length = myBranch.SerializableGrowthValue + myBranch.tipRadius;
        greybox.transform.localScale = new Vector3(width * widthModifier, length, 1);
        greybox.transform.localPosition = new Vector3(0, length / 2, 0);

        if (!colorInitialized && myBranch.myRoot.player != null)
        {
            colorInitialized = true;
            greybox.GetComponent<Renderer>().material.color = myBranch.myRoot.player.playerColor;
        }

        int index = 0;
        foreach(Branch.BurnEvent be in burnEvents)
        {
            float burnlength = Mathf.Min(1, be.progress);
            float center = (Mathf.Min(1, be.start + be.progress) + Mathf.Max(0, be.start - be.progress)) * 0.5f;
            burnBoxes[index].transform.localScale = new Vector3(width * widthModifier, burnlength * myBranch.SerializableGrowthValue, 1);
            burnBoxes[index].transform.localPosition = new Vector3(0, center * myBranch.SerializableGrowthValue, -1);
            index++;
        }
    }

    public void SetWidthModifier(float modifier)
    {
        widthModifier = modifier;
    }
}
