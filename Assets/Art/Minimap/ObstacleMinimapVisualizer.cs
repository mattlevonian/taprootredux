﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Obstacle))]
public class ObstacleMinimapVisualizer : MonoBehaviour
{
    [SerializeField]
    private Renderer targetRenderer;

    [SerializeField]
    private Material minimapMaterial;

    private void Start()
    {
        GameObject obj = new GameObject("minimap obj");
        obj.transform.SetParent(this.transform, false);
        MeshRenderer r = obj.AddComponent<MeshRenderer>();
        r.sharedMaterial = minimapMaterial;
        MeshFilter f = obj.AddComponent<MeshFilter>();
        f.sharedMesh = targetRenderer.GetComponent<MeshFilter>().sharedMesh;

        obj.layer = GameManager.MinimapLayer;
    }
}
