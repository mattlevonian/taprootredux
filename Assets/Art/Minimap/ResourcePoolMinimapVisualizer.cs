﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ResourcePool))]
public class ResourcePoolMinimapVisualizer : MonoBehaviour
{
    [SerializeField]
    private Renderer targetRenderer;

    [SerializeField]
    private Material minimapMaterial;

    private ResourcePool pool;

    private Renderer r;

    private void Start()
    {
        pool = GetComponent<ResourcePool>();

        GameObject obj = new GameObject("minimap obj");
        obj.transform.SetParent(this.transform, false);
        r = obj.AddComponent<MeshRenderer>();
        r.material = minimapMaterial;
        MeshFilter f = obj.AddComponent<MeshFilter>();
        f.sharedMesh = targetRenderer.GetComponent<MeshFilter>().sharedMesh;

        obj.layer = GameManager.MinimapLayer;
    }

    private void Update()
    {
        if (r.material != null && pool != null)
        {
            r.material.color = Color.Lerp(Color.gray, Color.blue, pool.FractionLeft);
        }
    }
}
