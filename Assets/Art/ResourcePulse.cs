﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcePulse : MonoBehaviour
{
    [SerializeField]
    private float flowRate;

    public void Fire(Branch startBranch, Branch endBranch)
    {
        StartCoroutine(EffectRoutine(startBranch, endBranch));
    }
    public void Fire(Branch startBranch)
    {
        StartCoroutine(EffectRoutine(startBranch, null));
    }

    private IEnumerator EffectRoutine(Branch startBranch, Branch endBranch)
    {
        Branch currentBranch = startBranch;
        while (true)
        {
            // move from start of a current branch to base
            float progress = 1;
            while(progress >= 0)
            {
                if(currentBranch == null) // if the branch is destroyed mid-animation
                {
                    Destroy(this.gameObject);
                    yield break;
                }
                transform.position = currentBranch.transform.position + progress * currentBranch.SerializableGrowthValue * currentBranch.worldGrowDirection + Vector3.back;
                progress -= Time.deltaTime * flowRate / currentBranch.SerializableGrowthValue;
                yield return null;
            }

            // at end, move to next branch or stop
            if (currentBranch == null || currentBranch.parent == null || currentBranch.parent.Equals(endBranch))
            {
                Destroy(this.gameObject);
                yield break;
            }
            else
            {
                currentBranch = currentBranch.parent;
            }
        }
    }
}
