﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible for encapsulating everything to do with visualizing a branch. 
/// Burning visuals, growth, chopping, it all goes here.
/// 
/// TODO expand to actually look like a root and be pretty
/// </summary>
[RequireComponent(typeof(Branch))]
public class BranchVisualizer : MonoBehaviour
{
    private Branch myBranch;

    [SerializeField]
    private GameObject greybox;

    // when the branch is chopped along its length, chopped becomes true and upper segment is spawned
    private bool chopped;
    private float chopPoint;
    private GameObject upperSegment;

    [SerializeField]
    private float width;

    private float widthModifier = 1;

    private bool colorInitialized;


    private List<Branch.BurnEvent> burnEvents = new List<Branch.BurnEvent>();
    private List<GameObject> burnBoxes = new List<GameObject>();

    private void BurnEventAdded(Branch.BurnEvent be)
    {
        burnEvents.Add(be);
        burnBoxes.Add(Instantiate(greybox, greybox.transform.parent, false));
        burnBoxes[burnBoxes.Count - 1].GetComponent<Renderer>().material.color = Color.red;
    }


    private void Start()
    {
        myBranch = GetComponent<Branch>();

        greybox.transform.localScale = new Vector3(width * widthModifier, myBranch.tipRadius, 1);
        greybox.transform.localPosition = new Vector3(0, myBranch.tipRadius / 2, 0);

        if (myBranch.myRoot.player != null)
        {
            colorInitialized = true;
            greybox.GetComponent<Renderer>().material.color = myBranch.myRoot.player.playerColor;
        }


        myBranch.newBurnEventAdded += BurnEventAdded;
        myBranch.onChopped += ChopAtPoint;
    }

    private void Update()
    {
        float length = myBranch.SerializableGrowthValue + myBranch.tipRadius;

        if (!chopped)
        {
            greybox.transform.localScale = new Vector3(width * widthModifier, length, 1);
            greybox.transform.localPosition = new Vector3(0, length / 2, 0);
        }
        else
        {
            float gapWidth = width * 1.75f;
            float upperPos = Mathf.Min(1, chopPoint + gapWidth / 2);
            float lowerPos = Mathf.Max(0, chopPoint - gapWidth / 2);

            upperSegment.transform.localScale = new Vector3(width * widthModifier, (length - upperPos), 1);
            upperSegment.transform.localPosition = new Vector3(0, (length - upperPos) / 2 + upperPos, 0);

            greybox.transform.localScale = new Vector3(width * widthModifier, lowerPos, 1);
            greybox.transform.localPosition = new Vector3(0, lowerPos / 2, 0);
        }

        if (!colorInitialized && myBranch.myRoot.player != null)
        {
            colorInitialized = true;
            greybox.GetComponent<Renderer>().material.color = myBranch.myRoot.player.playerColor;
        }


        int index = 0;
        foreach (Branch.BurnEvent be in burnEvents)
        {
            float burnlength = Mathf.Min(1, be.progress);
            float center = (Mathf.Min(1, be.start + be.progress) + Mathf.Max(0, be.start - be.progress)) * 0.5f;
            burnBoxes[index].transform.localScale = new Vector3(width * widthModifier, burnlength * myBranch.SerializableGrowthValue, 1);
            burnBoxes[index].transform.localPosition = new Vector3(0, center * myBranch.SerializableGrowthValue, -1);
            index++;
        }
    }

    public void ChopAtPoint(float pos)
    {
        chopped = true;
        chopPoint = pos;
        upperSegment = Instantiate(greybox, greybox.transform.parent, false);
    }

    public void SetWidthModifier(float modifier)
    {
        widthModifier = modifier;
    }
}
