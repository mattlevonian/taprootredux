﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a segment of a root. Starts where it was branched off or at the root, 
/// and ends where the root was subsequently branched again.
/// The position of this object marks the base.
/// </summary>
public class Branch : MonoBehaviour
{
    #region Branch Hierarchy References

    /// <summary>
    /// If this is null, the branch ends in a growing tip. 
    /// Once the branch splits, the primary child continues growing in the original direction.
    /// </summary>
    internal Branch primaryChild;
    
    /// <summary>
    /// If this is null, the branch ends in a growing tip. 
    /// Once the branch splits, the secondary child grows in a newly branched direction.
    /// </summary>
    internal Branch secondaryChild;

    /// <summary>
    /// The branch that this came from. If its null, then it is the starting branch of a root.
    /// </summary>
    internal Branch parent;

    /// <summary>
    /// Specified in the starting branch by the root system, and passed on to its children.
    /// </summary>
    internal Root myRoot;

    #endregion

    #region Branch GUID and Directory

    public static void ResetGUID()
    {
        nextGUID = 1;
    }

    private static int nextGUID = 1;
    // used to refer to branches across the network
    internal int GUID { get; private set; }

    // stores by GUID
    public static Dictionary<int, Branch> branchDirectory = new Dictionary<int, Branch>();

    public static Branch Find(int id)
    {
        return branchDirectory[id];
    }

    private void Awake()
    {
        GUID = nextGUID;
        nextGUID++;
        branchDirectory.Add(GUID, this);
    }

    #endregion


    internal Vector3 EndPositionInWorld
    {
        get
        {
            // return the position of the tip or end of this branch in world space
            return transform.position + growAmount * worldGrowDirection;
        }
    }
    internal Vector3 StartPositionInWorld
    {
        get
        {
            return transform.position;
        }
    }

    #region Abilities

    [SerializeField]
    private float fastGrowSpeed;

    private bool armored;
    private bool cuttingEnabled;

    [SerializeField]
    private float fastDrainRate;

    [SerializeField, Tooltip("Normal drain rate. Overidden by Fast Drain Rate when Fast Drain ability is used.")]
    private float drainRate;

    internal float DrainRate
    {
        get
        {
            return drainRate;
        }
    }

    internal void EnableFastGrowth()
    {
        growSpeed = fastGrowSpeed;
    }

    internal void EnableFastDrain()
    {
        drainRate = fastDrainRate;
    }

    internal void EnableRootCutting()
    {
        cuttingEnabled = true;
    }

    internal void EnableArmor()
    {
        armored = true;
    }

    internal void DisableCutting()
    {
        cuttingEnabled = false;
    }

    #endregion

    public bool CanBeSeenByLocalPlayer()
    {
        foreach(Player p in Player.LocalPlayers)
        {
            if(p != null)
            {
                if (!p.viewContext.ui.BranchOutsideOfScreen(this))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public bool IsSelfOrSiblingOrParent(GameObject gameObject)
    {
        if(gameObject.Equals(this.gameObject))
        {
            return true;
        }
        if(parent != null)
        {
            if(gameObject.Equals(this.parent.gameObject))
            {
                return true;
            }
            if (gameObject.Equals(this.parent.primaryChild.gameObject) || gameObject.Equals(this.parent.secondaryChild.gameObject))
            {
                return true;
            }
        }
        return false;
    }

    // tip variables
    [SerializeField]
    private BranchTip branchTipPrefab;
    private BranchTip myTip;
    internal int myTipIndex; // the index that our tip is stored under in our parent Root

    [SerializeField]
    internal float tipRadius;

    // growth variables
    private bool growing; // should we actively increase our growth in Update?
    private float growAmount; // units grown in the grow direction
    internal Vector3 worldGrowDirection;
    [SerializeField]
    private float growSpeed;
    [SerializeField, Tooltip("If the branch is the actively controlled tip, it grows faster by this much")]
    private float activeGrowthModifier;

    public bool isGrowing
    {
        get
        {
            return growing;
        }
    }

    // branch variables
    [SerializeField]
    private Branch branchPrefab;
    [SerializeField, Tooltip("How long does the player need to wait before they can branch again?")]
    private float minTimeToBranch;
    private float growStartTime;
    [SerializeField, Tooltip("Minimum angle with current grow direction where a branch is permitted")]
    private float minAngle;
    [SerializeField, Tooltip("Maximum angle against current grow direction where a branch is permitted")]
    private float maxAngle;


    [SerializeField]
    private EdgeCollider2D edgeCollider;
    private Vector2[] colliderPoints = new Vector2[2];
 
    /// <summary>
    /// stores a reference to whatever pool this branch is draining
    /// </summary>
    private Pool currentPool;

    /// <summary>
    /// if we collided with a branch, its reference is stored here
    /// </summary>
    private Branch collidedBranch;

    private class ImpactEvent
    {
        public Vector3 point;
        public Branch branch;
        public bool fireTransfered;
    }
    private List<ImpactEvent> impacts = new List<ImpactEvent>();

    public void ImpactedByBranch(Branch b)
    {
        ImpactEvent e = new ImpactEvent();
        e.point = b.EndPositionInWorld;
        e.branch = b;
        impacts.Add(e);

        if (myRoot.player.isLocal)
        {
            if (myRoot.player.viewContext != null && myRoot.isFocused)
            {
                myRoot.player.viewContext.ImpactedVisuals(this);
            }
            SoundManager.Instance.PlaySound(SoundName.BranchImpactedByBranch);
        }
    }

    internal float SerializableGrowthValue
    {
        get
        {
            // return a value that allows us to sync growth amount over the network
            return growAmount;
        }
    }

    public void StartGrowing()
    {
        growing = true;
        growStartTime = Time.time;

        colliderPoints[0] = Vector3.zero;
        colliderPoints[1] = Vector3.zero;

        if (myRoot.player.isLocal)
        {
            myTip = Instantiate(branchTipPrefab);
            myTip.SetRadius(tipRadius);
            myTip.myBranch = this;
        }
    }

    [SerializeField, Tooltip("How fast fire burns along the length of the branch, in units per second")]
    private float burnRate;

    [SerializeField]
    private GameObject burnEffectPrefab;

    public class BurnEvent
    {
        public float start; // a value between 0 and 1, where 0 is the base and 1 is the tip of the branch
        public float progress; // a value between 0 and 1, where 0 is no spreading, and 1 is spreading correspondent to the length of the branch

        public GameObject upEffect;
        public GameObject downEffect;
    }
    private List<BurnEvent> burnEvents = new List<BurnEvent>();

    public event Action<BurnEvent> newBurnEventAdded;

    // called across the network when this branch catches on fire (even if it happened locally)
    // also called locally when spreading fire from other branches
    public void CatchFire(float start, bool first)
    {
        // root only catches fire once, the first time
        if (first)
        {
            myRoot.player.RootCaughtFire(myRoot.rootIndex);
        }
        
        // catch fire and start burning
        BurnEvent be = new BurnEvent();
        be.start = start;
        be.downEffect = Instantiate(burnEffectPrefab);
        be.upEffect = Instantiate(burnEffectPrefab);
        burnEvents.Add(be);

        newBurnEventAdded.Invoke(be);

        if (start >= 1)
        {
            Destroy(be.upEffect);
            spreadToChildren = true;
        }

        if (start <= 0)
        {
            Destroy(be.downEffect);
            spreadToParent = true;
        }

        UpwardsRecursiveStopGrowing(true);
    }

    /// <summary>
    /// Called by a branch that has collided with us when it wants to set us on fire.
    /// We search all our impact events for that branch and start a fire at that point.
    /// </summary>
    /// <param name="branch"></param>
    public void CatchFire(Branch branch)
    {
        foreach(ImpactEvent e in impacts)
        {
            if(e.branch.Equals(branch))
            {
                if (e.fireTransfered)
                {
                    return;
                }

                if (myRoot.player.viewContext != null && myRoot.isFocused)
                {
                    myRoot.player.viewContext.CatchFireVisuals(this);
                }

                e.fireTransfered = true;
                CatchFire(Vector3.Dot(worldGrowDirection, e.point - transform.position) / growAmount, true);
                return;
            }
        }
    }

    public event Action<float> onChopped;

    private bool chopped;

    public void ChopAtPoint(Vector3 point)
    {
        chopped = true;

        UpwardsRecursiveStopGrowing(false);

        if (myRoot.player.viewContext != null && myRoot.isFocused)
        {
            myRoot.player.viewContext.ChoppedVisuals();
        }

        // chopping visuals
        float pos = Vector3.Dot((point - transform.position), worldGrowDirection);

        if (onChopped != null)
        {
            onChopped.Invoke(pos);
        }
    }

    private void UpwardsRecursiveStopGrowing(bool fromFire)
    {
        growing = false;

        if (myTip != null)
        {
            Destroy(myTip.gameObject);
        }

        if(currentPool != null)
        {
            currentPool.RemoveBranch(this);
        }

        if (fromFire)
        {
            if (myRoot.player.viewContext != null && myRoot.isFocused)
            {
                myRoot.player.viewContext.CatchFireVisuals(this, true);
            }
        }

        if (primaryChild != null)
        {
            primaryChild.UpwardsRecursiveStopGrowing(fromFire);
        }
        if (secondaryChild != null)
        {
            secondaryChild.UpwardsRecursiveStopGrowing(fromFire);
        }
    }

    private bool spreadToChildren;
    private bool spreadToParent;

    public bool isBurning
    {
        get
        {
            return burnEvents.Count > 0;
        }
    }

    [SerializeField]
    private ResourcePulse pulsePrefab;
    private float amountDrained;
    private float lastPulse;
    [SerializeField]
    private float pulseAmount;

    // immediately transfers added amount up the chain to the root. 
    // if the branch is burning or chopped, the transfer will be aborted.
    // however, we also keep track of how much has been transfered and periodically
    // send visual "pulses" to indicate the resource drain the player
    public bool AddResources(float amount, Branch startingBranch)
    {
        if (!isBurning && !chopped)
        {
            if (parent != null)
            {
                return parent.AddResources(amount, startingBranch);
            }
            else
            {
                myRoot.AddResources(amount);

                amountDrained += amount;
                if (amountDrained - lastPulse > pulseAmount)
                {
                    lastPulse = amountDrained;
                    // create resource drain effect
                    ResourcePulse pulse = Instantiate(pulsePrefab);
                    pulse.Fire(startingBranch);
                }

                return true;
            }
        }
        else
        {
            amountDrained += amount;
            if (amountDrained - lastPulse > pulseAmount)
            {
                lastPulse = amountDrained;
                // create aborted resource effect
                ResourcePulse pulse = Instantiate(pulsePrefab);
                pulse.Fire(startingBranch, this);
            }

            return false;
        }
    }

    private void Update()
    {
        // grow and update tip
        if (growing)
        {
            growAmount += Time.deltaTime * growSpeed * ((myRoot.ActiveTipIndex == this.myTipIndex) ? activeGrowthModifier : 1);
        }
        else if (isBurning)
        {
            foreach (BurnEvent burnEvent in burnEvents)
            {
                burnEvent.progress += burnRate * Time.deltaTime / growAmount;

                // TODO set mesh burning visuals

                // update burning visuals
                if (burnEvent.upEffect != null)
                    burnEvent.upEffect.transform.position = transform.position + worldGrowDirection * growAmount * (burnEvent.start + burnEvent.progress) + Vector3.back;
                if (burnEvent.downEffect != null)
                    burnEvent.downEffect.transform.position = transform.position + worldGrowDirection * growAmount * (burnEvent.start - burnEvent.progress) + Vector3.back;

                if (burnEvent.start + burnEvent.progress > 1 && !spreadToChildren)
                {
                    spreadToChildren = true;
                    Destroy(burnEvent.upEffect);

                    // spread to children
                    if (primaryChild != null)
                    {
                        primaryChild.CatchFire(0, false);
                    }
                    if (secondaryChild != null)
                    {
                        secondaryChild.CatchFire(0, false);
                    }

                    // transfer fire to branches we have collided with
                    if (collidedBranch != null)
                    {
                        collidedBranch.CatchFire(this);
                    }
                }

                // transfer fire to branches that have collided with us
                foreach (ImpactEvent impact in impacts)
                {
                    if (!impact.fireTransfered && Mathf.Abs(burnEvent.start - (Vector3.Dot(worldGrowDirection, impact.point - transform.position) / growAmount)) < burnEvent.progress)
                    {
                        impact.fireTransfered = true;
                        impact.branch.CatchFire(1, false);
                    }
                }

                if (burnEvent.start - burnEvent.progress < 0 && !spreadToParent)
                {
                    spreadToParent = true;
                    Destroy(burnEvent.downEffect);

                    // spread to parent and sibling
                    if (parent != null)
                    {
                        parent.CatchFire(1, false);

                        // spread to sibling
                        if (parent.secondaryChild == this)
                        {
                            parent.primaryChild.CatchFire(0, false);
                        }
                        else if (parent.primaryChild == this)
                        {
                            parent.secondaryChild.CatchFire(0, false);
                        }
                    }
                    else
                    {
                        myRoot.FireReachedBase();
                        break;
                    }
                }
            }

            foreach (BurnEvent burnEvent in burnEvents)
            {
                foreach (BurnEvent other in burnEvents)
                {
                    if (other.Equals(burnEvent)) continue;

                    if (burnEvent.start < other.start)
                    {
                        if (burnEvent.upEffect == null) continue;
                        if(burnEvent.start + burnEvent.progress > other.start - other.progress)
                        {
                            Destroy(burnEvent.upEffect);
                        }
                    }
                    else
                    {
                        if (burnEvent.downEffect == null) continue;
                        if (burnEvent.start - burnEvent.progress < other.start + other.progress)
                        {
                            Destroy(burnEvent.downEffect);
                        }
                    }
                }
            }
        }
    }

    private void FixedUpdate()
    {
        colliderPoints[1] = transform.InverseTransformDirection(growAmount * worldGrowDirection);
        edgeCollider.points = colliderPoints;
    }

    /// <summary>
    /// Called by the branch tip object, which is actively monitoring for collisions
    /// </summary>
    /// <param name="impactedObstacle">The object we impacted, could be a Branch or an Obstacle</param>
    public void OnTipImpact(GameObject impactedObstacle)
    {
        if (myTip != null) // this path is taken the first time it is called on the owning client
        {
            if (cuttingEnabled)
            {
                Branch hitBranch = impactedObstacle.GetComponent<Branch>();
                if (hitBranch != null && !hitBranch.armored && (GameManager.AllowChoppingOwnRoots || hitBranch.myRoot.player.playerIndex != myRoot.player.playerIndex))
                {
                    Debug.Log("chopping a branch");
                    cuttingEnabled = false;
                    myRoot.DisableCutting();
                    myRoot.player.CutOtherBranch(hitBranch.GUID, EndPositionInWorld);
                    return;
                }
            }

            Destroy(myTip.gameObject);
            myTip = null;

            myRoot.player.BranchCollided(myRoot.rootIndex, myTipIndex, EndPositionInWorld, impactedObstacle);
        }
        else // this path is followed the second time it is called (from the RPC), as well as on other clients
        {
            growing = false;

            if(myRoot.player.viewContext != null && myRoot.isFocused)
            {
                myRoot.player.viewContext.CollisionVisuals(this);
            }

            // perhaps start draining a pool, or catch on fire
            if (impactedObstacle != null && impactedObstacle.GetComponentInParent<Pool>() != null)
            {
                Debug.Log("impacted a pool");
                currentPool = impactedObstacle.GetComponentInParent<Pool>();
                currentPool.AddBranch(this);

                if(myRoot.player.isLocal || CanBeSeenByLocalPlayer())
                {
                    if (currentPool.GetType() == typeof(ResourcePool))
                    {
                        if (myRoot.player.viewContext != null && myRoot.isFocused)
                        {
                            myRoot.player.viewContext.ResourcesVisuals(this);
                        }
                        SoundManager.Instance.PlaySound(SoundName.BranchImpactsResourcePool);
                    }
                    else if (currentPool.GetType() == typeof(FirePool))
                    {
                        if (myRoot.player.viewContext != null && myRoot.isFocused)
                        {
                            myRoot.player.viewContext.CatchFireVisuals(this);
                        }
                        SoundManager.Instance.PlaySound(SoundName.BranchImpactsFirePool);
                    }
                }
            }

            else if (impactedObstacle != null && impactedObstacle.GetComponentInParent<Branch>() != null)
            {
                Debug.Log("impacted a branch");
                collidedBranch = impactedObstacle.GetComponentInParent<Branch>();
                collidedBranch.ImpactedByBranch(this);

                if (myRoot.player.isLocal || (!collidedBranch.myRoot.player.isLocal && CanBeSeenByLocalPlayer()))
                {
                    SoundManager.Instance.PlaySound(SoundName.BranchImpactsBranch);
                }
            }

            else
            {
                Debug.Log("impacted an obstacle");

                if (myRoot.player.isLocal || CanBeSeenByLocalPlayer())
                {
                    SoundManager.Instance.PlaySound(SoundName.BranchImpactsBranch);
                }
            }
        }

        // note, this area may be skipped - don't put code here
    }

    public void SetParent(Branch myParent)
    {
        parent = myParent;
        myRoot = parent.myRoot;
    }

    public void ForceEndToWorldPosition(Vector3 branchEnd)
    {
        // forcefully set the branch tip to this position (or the closest approximation along the original grow direction)
        growAmount = Vector3.Dot((branchEnd - transform.position), worldGrowDirection);

        // TODO update visuals and everything else
    }

    public void ForceEndToGrowthValue(float growthValue)
    {
        //if (growing) // check in case we recieve some serialized value after finalizing its position from a split RPC
        {
            // forcefully set the branch tip to the position indicated by this growth factor
            growAmount = growthValue;

            // TODO update visuals and everything else
        }
    }

    public bool AllowedToBranch(Vector3 worldDirection)
    {
        if (growing)
        {
            // make sure there is a minimum branch time
            if (Time.time - growStartTime > minTimeToBranch)
            {
                if (worldDirection.magnitude < PlayerController.DirectionDeadZone) return false;

                float angle = Mathf.Abs(Vector3.Angle(worldDirection, worldGrowDirection));
                return angle >= minAngle && angle <= maxAngle;
            }
        }

        return false;
    }

    public void CreateBranchInWorldDirection(Vector3 direction)
    {
        // stop growing and create children
        growing = false;
        if (myTip != null)
        {
            Destroy(myTip.gameObject);
        }

        // set their root system and parent values, and grow direction
        primaryChild = Instantiate(branchPrefab, EndPositionInWorld, Quaternion.LookRotation(Vector3.forward, worldGrowDirection));
        primaryChild.worldGrowDirection = worldGrowDirection;
        primaryChild.myRoot = myRoot;
        primaryChild.parent = this;
        primaryChild.drainRate = this.drainRate;
        primaryChild.growSpeed = this.growSpeed;
        primaryChild.armored = this.armored;
        primaryChild.cuttingEnabled = false;
        primaryChild.StartGrowing();

        secondaryChild = Instantiate(branchPrefab, EndPositionInWorld, Quaternion.LookRotation(Vector3.forward, direction));
        secondaryChild.worldGrowDirection = direction;
        secondaryChild.myRoot = myRoot;
        secondaryChild.parent = this;
        secondaryChild.drainRate = this.drainRate;
        secondaryChild.growSpeed = this.growSpeed;
        secondaryChild.armored = this.armored;
        secondaryChild.cuttingEnabled = this.cuttingEnabled;
        secondaryChild.StartGrowing();
    }

    public void Cut()
    {
        // stop growing, stop sucking nutrients
        growing = false;
        if(myTip != null)
        {
            Destroy(myTip.gameObject);
        }
        if (currentPool != null)
        {
            currentPool.RemoveBranch(this);
        }

        // stop burning
        foreach(BurnEvent e in burnEvents)
        {
            Destroy(e.upEffect);
            Destroy(e.downEffect);
        }
        burnEvents.Clear();

        if (primaryChild != null)
        {
            primaryChild.Cut();
        }
        if (secondaryChild != null)
        {
            secondaryChild.Cut();
        }
    }

    public void SetWidthModifier(float modifier)
    {
        GetComponent<BranchVisualizer>().SetWidthModifier(modifier);

        if (primaryChild != null)
        {
            primaryChild.SetWidthModifier(modifier);
        }
        if (secondaryChild != null)
        {
            secondaryChild.SetWidthModifier(modifier);
        }
    }

    public void RecursiveDestroy()
    {
        if (primaryChild != null)
        {
            primaryChild.RecursiveDestroy();
        }
        if (secondaryChild != null)
        {
            secondaryChild.RecursiveDestroy();
        }

        Destroy(this.gameObject);
    }
}
