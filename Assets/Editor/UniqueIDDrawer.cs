﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomPropertyDrawer(typeof(UniqueID))]
public class UniqueIDDrawer : PropertyDrawer
{

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        EditorGUI.BeginProperty(position, label, property);
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;
        string value = property.FindPropertyRelative("ID").intValue.ToString();
        Rect IDRect = new Rect(position.x, position.y, 25f + value.Length * 6f, position.height + 2f);
        EditorGUI.LabelField(IDRect, value, GUI.skin.box);
        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();

    }

}