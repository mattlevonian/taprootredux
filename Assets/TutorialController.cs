﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The phases are:
/// -sapling
/// -splitting
/// -resources
/// -catching on fire / cutting
/// -picking ability / new sapling
/// </summary>
public class TutorialController : MonoBehaviour
{
    public event Action tutorialCompleted;

    private void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void Init()
    {

    }

    public void EndTutorial()
    {
        if(tutorialCompleted != null)
        {
            tutorialCompleted.Invoke();
        }
        Destroy(this.gameObject);
    }
}
