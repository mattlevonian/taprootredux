﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData : MonoBehaviour
{
    private static MapData _instance;
    public static MapData Instance
    {
        get
        {
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }

    [System.Serializable]
    private struct PlayerSlot
    {
        public Transform cameraDirection;
        public Transform[] rootSlots;
    }
    [SerializeField]
    private PlayerSlot[] playerSlots;

    public int NumRootSlots
    {
        get
        {
            return playerSlots[0].rootSlots.Length;
        }
    }

    public int NumPlayerSlots
    {
        get
        {
            return playerSlots.Length;
        }
    }

    /// <summary>
    /// Returns the "up" direction for a slot
    /// </summary>
    /// <param name="playerIndex">The index of the player slot to use</param>
    /// <returns>A world-space vector indicating the "up" for the player's camera</returns>
    public Vector3 GetPlayerFacing(int playerIndex)
    {
        if (playerIndex > playerSlots.Length - 1)
        {
            Debug.LogError("Player index out of bounds!");
            return Vector3.zero;
        }

        return playerSlots[playerIndex].cameraDirection.up;
    }

    /// <summary>
    /// Gets the starting position for a particular root slot for a particular player
    /// </summary>
    /// <param name="playerIndex"></param>
    /// <param name="rootIndex"></param>
    /// <param name="worldPosition"></param>
    /// <param name="worldFacing"></param>
    public void GetRootStartForPlayer(int playerIndex, int rootIndex, out Vector3 worldPosition, out Vector3 worldFacing)
    {
        if (playerIndex > playerSlots.Length - 1)
        {
            Debug.LogError("Player index out of bounds!");
            worldPosition = Vector3.zero;
            worldFacing = Vector3.zero;
            return;
        }
        if (rootIndex > playerSlots[playerIndex].rootSlots.Length - 1)
        {
            Debug.LogError("Root index out of bounds!");
            worldPosition = Vector3.zero;
            worldFacing = Vector3.zero;
            return;
        }

        worldPosition = playerSlots[playerIndex].rootSlots[rootIndex].position;
        worldFacing = playerSlots[playerIndex].rootSlots[rootIndex].up;
    }
}
