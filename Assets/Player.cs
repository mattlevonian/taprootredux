﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a player's information and acts as a conduit for control calls across the network.
/// The player action methods (which correspond to buffered network actions) are called by some sort of input controller.
/// </summary>
public class Player : MonoBehaviour
{
    #region Player Accounting

    public static Player Find(int index)
    {
        return Players[index];
    }


    public const int MaxNumPlayers = 4;

    public static int NumPlayers
    {
        get
        {
            for (int i = 0; i < Players.Length; i++)
            {
                if (Players[i] == null)
                {
                    return i;
                }
            }
            return Players.Length;
        }
    }

    public static Player[] Players = new Player[MaxNumPlayers];
    public static List<Player> LocalPlayers = new List<Player>();

    public static Color[] PlayerColors = new Color[] { Color.yellow, Color.cyan, Color.white, Color.magenta };

    #endregion

    // --------- SYNCED PROPERTIES ---------

    #region Synced Properties

    /// <summary>
    /// Important distinction from PhotonView ID. A client can have between 1 and 4 local players.
    /// </summary>
    internal int playerIndex = -1;

    /// <summary>
    /// Whether or not the player is readied in the lobby
    /// </summary>
    private bool ready;

    /// <summary>
    /// The current root the player is focusing on
    /// </summary>
    private int activeRootIndex = -1;

    // TODO not actually directly settable or synced, only implicit
    internal string playerName;

    // TODO not actually directly settable or synced, only implicit
    internal Color playerColor;

    #endregion

    internal bool isReady
    { get { return ready; } }

    internal bool isLocal
    { get { return PhotonView.Get(this).isMine; } }

    // -------------------------------------

    internal ViewContext viewContext;

    #region Events for UI

    // called locally when the ready status of this player changes
    public event Action<bool> onReady;
    public event Action onInfoUpdate;

    public event Action<int> onRootDies;
    public event Action<int> onRootStartsRespawning;
    public event Action<int> onRootStartsGrowing;
    public event Action<int> onRootCatchFire;
    public event Action<int> onRootReadyToGrow; 
    public event Action<int> onChangeActiveRoot;

    internal void RootCaughtFire(int rootIndex)
    {
        if (onRootCatchFire != null)
        {
            onRootCatchFire.Invoke(rootIndex);
        }
    }

    #endregion

    #region Root, Branch, and Tip Accessors

    // roots
    private Root[] myRoots;

    [SerializeField]
    private string rootPrefabName;

    internal Bounds ActiveRootBounds
    {
        get
        {
            if (myRoots[activeRootIndex] != null)
            {
                return myRoots[activeRootIndex].worldRect;
            }
            else
            {
                return new Bounds(GetActiveTipPosition(), Vector3.zero);
            }
        }
    }

    internal int ActiveRootAbility
    {
        get
        {
            if (myRoots[activeRootIndex] != null && myRoots[activeRootIndex].ActiveTipIsGrowing)
            {
                return myRoots[activeRootIndex].AbilityIndex;
            }
            else
            {
                return 0;
            }
        }
    }

    /// <summary>
    /// Calls down to the active tip and gets its position in world space
    /// </summary>
    /// <returns>The position of the active tip in world space</returns>
    public Vector3 GetActiveTipPosition()
    {
        if (activeRootIndex >= 0)
        {
            if (myRoots[activeRootIndex] != null)
            {
                return myRoots[activeRootIndex].GetActiveTipPosition();
            }
            else
            {
                Vector3 pos, fac;
                MapData.Instance.GetRootStartForPlayer(playerIndex, activeRootIndex, out pos, out fac);
                return pos;
            }
        }
        else
        {
            return Vector3.zero;
        }
    }

    public bool ActiveTipIsGrowing
    {
        get
        {
            if (activeRootIndex < 0 || myRoots[activeRootIndex] == null) return false;
            return myRoots[activeRootIndex].ActiveTipIsGrowing;
        }
    }


    internal List<Branch> GrowingBranches
    {
        get
        {
            List<Branch> branches = new List<Branch>();
            foreach(Root r in myRoots)
            {
                if (r != null)
                {
                    branches.AddRange(r.GrowingBranches);
                }
            }
            return branches;
        }
    }

    #endregion

    #region Device Tooltip Info

    // used for showing the correct tooltips
    // set by the PlayerController when it attaches
    private InControl.InputDeviceClass _deviceClass;
    internal InControl.InputDeviceClass DeviceClass
    {
        get
        {
            return _deviceClass;
        }
        set
        {
            _deviceClass = value;

            if (onInfoUpdate != null)
            {
                onInfoUpdate.Invoke();
            }
        }
    }
    private InControl.InputDeviceStyle _deviceStyle;
    internal InControl.InputDeviceStyle DeviceStyle
    {
        get
        {
            return _deviceStyle;
        }
        set
        {
            _deviceStyle = value;

            if (onInfoUpdate != null)
            {
                onInfoUpdate.Invoke();
            }
        }
    }

    #endregion

    #region Score Management

    public void AddResources(float amount)
    {
        GameManager.Instance.GlobalScoreKeeper.AddToScore(playerIndex, amount);
    }

    public float Score
    {
        get
        {
            return GameManager.Instance.GlobalScoreKeeper.GetScoreByPlayerIndex(playerIndex);
        }
    }

    #endregion

    #region Player Setup and Cleanup

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if(PhotonNetwork.isMasterClient)
        {
            PhotonView.Get(this).RPC("RpcSetPlayerIndex", PhotonTargets.AllBuffered, NumPlayers);
        }
        if(PhotonView.Get(this).isMine)
        {
            LocalPlayers.Add(this);

            for(int i = 0; i < LocalPlayers.Count; i++)
            {
                if(LocalPlayers[i] == null)
                {
                    LocalPlayers.RemoveAt(i);
                    i--;
                }
            }
        }

        GameManager.Instance.UpdatePlayerList();
    }

    // called at the beginning to sync player index from the master client to all clients
    [PunRPC]
    private void RpcSetPlayerIndex(int newIndex)
    {
        if (playerIndex >= 0)
        {
            Players[playerIndex] = null;
        }
        playerIndex = newIndex;
        Players[playerIndex] = this;

        Debug.Log(playerIndex.ToString() + PlayerColors[playerIndex].ToString());
        playerColor = PlayerColors[playerIndex];
        playerName = "Player " + playerIndex;
        gameObject.name = playerName;

        if(onInfoUpdate != null)
        {
            onInfoUpdate.Invoke();
        }

        GameManager.Instance.UpdatePlayerList();
    }

    private void OnDestroy()
    {
        Players[playerIndex] = null;
        GameManager.Instance.UpdatePlayerList();
    }

    #endregion

    #region Game Session Setup and Cleanup

    // called immediately on game scene load
    public void Setup()
    {
        myRoots = new Root[MapData.Instance.NumRootSlots];

        if (isLocal)
        {
            CreateNewRoot(0);
            SwitchActiveBranch(0);

            if (viewContext != null)
            {
                viewContext.ui.ResetAbilityChooser();
                viewContext.ui.SetShowAbilityChooser(false);
            }
        }
    }

    // while playStarted is true, branching, targeting, cutting, and growing new roots can be done
    // when false, it essentially gates these RPCs
    private bool playStarted;

    // called at the finish of the pregame countdown
    public void StartPlay()
    {
        playStarted = true;

        if (isLocal)
        {
            StartGrowingActiveRoot();

            respawnRoutine = StartCoroutine(RegrowTimer());
        }
    }

    // called once time has run out
    public void StopPlay()
    {
        playStarted = false;

        if (isLocal)
        {
            StopCoroutine(respawnRoutine);

            if (viewContext != null)
            {
                Destroy(viewContext.gameObject);
            }
        }
    }

    #endregion

    // root regrowth
    #region Root Respawning

    [SerializeField]
    private float rootRespawnTime;

    public float GetRootRespawnTime(int rootIndex)
    {
        return rootRespawnTime;
    }

    private Coroutine respawnRoutine;
    private IEnumerator RegrowTimer()
    {
        while (true)
        {
            List<int> deadRoots = new List<int>();
            for (int i = 0; i < myRoots.Length; i++)
            {
                if (myRoots[i] == null)
                {
                    deadRoots.Add(i);
                }
            }

            if (deadRoots.Count > 0)
            {
                int index = UnityEngine.Random.Range(0, deadRoots.Count);

                if (onRootStartsRespawning != null)
                {
                    onRootStartsRespawning.Invoke(deadRoots[index]);
                }

                yield return new WaitForSeconds(rootRespawnTime);
            
                CreateNewRoot(deadRoots[index]);
            }
            else
            {
                yield return new WaitForSeconds(1);
            }
        }
    }

    #endregion

    #region Viewing Root and Minimap

    // changing views (root and minimap)
    private bool viewingRoot;
    private bool viewingMinimap;

    public void StartViewingRoot()
    {
        viewingRoot = true;
        if (viewContext != null)
        {
            viewContext.StartViewingRoot();
        }
    }
    public void StopViewingRoot()
    {
        viewingRoot = false;
        if (viewContext != null)
        {
            viewContext.StopViewingRoot();
        }
    }

    public void StartViewingMinimap()
    {
        viewingMinimap = true;
        if (viewContext != null)
        {
            viewContext.ui.ShowMinimap();
        }
    }
    public void StopViewingMinimap()
    {
        viewingMinimap = false;
        if (viewContext != null)
        {
            viewContext.ui.HideMinimap();
        }
    }

    #endregion

    #region Switching Root Focus

    public void RequestSwitchRootLeft()
    {
        if (!viewingRoot || viewingMinimap)
        {
            if (myRoots != null && myRoots[activeRootIndex] != null &&
               !myRoots[activeRootIndex].ActiveTipIsGrowing && myRoots[activeRootIndex].hasStarted)
            {
                ForceSwitchRootLeft();
            }
            else if(myRoots != null && myRoots[activeRootIndex] == null)
            {
                ForceSwitchRootLeft();
            }
            return;
        }
        ForceSwitchRootLeft();
    }
    public void ForceSwitchRootLeft()
    { 
        if (myRoots != null)
        {
            for(int i = activeRootIndex - 1; i >= 0; i--)
            {
                if(myRoots[i] != null)
                {
                    SwitchActiveBranch(i);
                    break;
                }
            }
        }
    }
    public void RequestSwitchRootRight()
    {
        if (!viewingRoot || viewingMinimap)
        {
            if(myRoots != null && myRoots[activeRootIndex] != null && 
                !myRoots[activeRootIndex].ActiveTipIsGrowing && myRoots[activeRootIndex].hasStarted)
            {
                ForceSwitchRootRight();
            }
            else if (myRoots != null && myRoots[activeRootIndex] == null)
            {
                ForceSwitchRootRight();
            }
            return;
        }
        ForceSwitchRootRight();
    }
    public void ForceSwitchRootRight()
    {
        if (myRoots != null)
        {
            for (int i = activeRootIndex + 1; i < myRoots.Length; i++)
            {
                if (myRoots[i] != null)
                {
                    SwitchActiveBranch(i);
                    break;
                }
            }
        }
    }

    #endregion

    // local player actions
    #region Local Player Actions

    // cutting
    [SerializeField]
    private float timeToCut;
    public bool AllowedToCut()
    {
        if (playStarted && activeRootIndex >= 0 && myRoots[activeRootIndex] != null && myRoots[activeRootIndex].hasStarted)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void StartCutting()
    {
        if (cuttingRoutine == null)
        {
            Debug.Log("start cutting");
            cuttingRoutine = StartCoroutine(CutTimer());
            viewContext.ui.StartCutting(timeToCut);
        }
    }
    public void StopCutting()
    {
        if (cuttingRoutine != null)
        {
            Debug.Log("stop cutting");
            StopCoroutine(cuttingRoutine);
            viewContext.ui.StopCutting();
            cuttingRoutine = null;
        }
    }
    private Coroutine cuttingRoutine;
    private IEnumerator CutTimer()
    {
        yield return new WaitForSeconds(timeToCut);
        this.CutBranch(activeRootIndex);
        cuttingRoutine = null;
    }


    /// <summary>
    /// Updates the UI to show where the player is aiming
    /// </summary>
    /// <param name="screenDirection">A vector where "up" is towards the top of the player's screen. 
    ///     A very low or 0 magnitude vector means the indicator should be hidden</param>
    public void SetTargetingDirection(Vector2 screenDirection)
    {
        if (playStarted && activeRootIndex >= 0 && myRoots[activeRootIndex] != null && myRoots[activeRootIndex].hasStarted && myRoots[activeRootIndex].ActiveTipIsGrowing)
        {
            if (!viewingRoot)
            {
                viewContext.ui.SetTargetingDirection(screenDirection);
            }
            else
            {
                viewContext.ui.SetTargetingDirection(Vector2.zero);
            }
        }
        else
        {
            viewContext.ui.SetTargetingDirection(Vector2.zero);
        }
    }

    public void SetAbilityChooseDirection(Vector2 screenDirection)
    {
        viewContext.ui.SetAbilityChooseDirection(screenDirection);
    }

    public void SetCameraZoom(float direction)
    {
        viewContext.SetCameraZoom(direction);
    }

    #endregion

    // networked Player actions
    #region Networked Player Action

    // LOBBY READY
    public void ToggleReady()
    {
        if (GameManager.Instance.InLobby)
        {
            PhotonView.Get(this).RPC("RpcSetReady", PhotonTargets.AllBuffered, !ready);
        }
    }
    public void SetReady(bool newReady)
    {
        PhotonView.Get(this).RPC("RpcSetReady", PhotonTargets.AllBuffered, newReady);
    }
    [PunRPC]
    private void RpcSetReady(bool newReady)
    {
        ready = newReady;

        GameManager.Instance.CheckReadyToStart();

        if (onReady != null)
        {
            onReady.Invoke(ready);
        }
    }

    // MAP SELECTION
    public void RequestMapUp()
    {
        if (GameManager.Instance.InLobby)
        {
            PhotonView.Get(this).RPC("RpcRequestMapUp", PhotonTargets.AllBufferedViaServer);
        }
    }
    [PunRPC]
    private void RpcRequestMapUp()
    {
        GameManager.Instance.MapUp();
    }
    public void RequestMapDown()
    {
        if (GameManager.Instance.InLobby)
        {
            PhotonView.Get(this).RPC("RpcRequestMapDown", PhotonTargets.AllBufferedViaServer);
        }
    }
    [PunRPC]
    private void RpcRequestMapDown()
    {
        GameManager.Instance.MapDown();
    }

    // SWITCHING
    private void SwitchActiveBranch(int rootIndex)
    {
        PhotonView.Get(this).RPC("RpcSwitchActiveBranch", PhotonTargets.AllBuffered, rootIndex);
    }
    [PunRPC]
    private void RpcSwitchActiveBranch(int rootIndex)
    {
        if (activeRootIndex >= 0 && myRoots[activeRootIndex] != null)
        {
            myRoots[activeRootIndex].isFocused = false;
        }

        activeRootIndex = rootIndex;

        if (activeRootIndex >= 0 && myRoots[activeRootIndex] != null)
        {
            myRoots[activeRootIndex].isFocused = true;
        }

        if (viewContext != null && myRoots[activeRootIndex] != null)
        {
            viewContext.ui.ResetAbilityChooser();
            viewContext.ui.SetShowAbilityChooser(!myRoots[activeRootIndex].hasStarted);
        }

        if (onChangeActiveRoot != null)
        {
            onChangeActiveRoot.Invoke(rootIndex);
        }

        // TODO glowing effect or something to indicate where the player is focused
    }

    // BRANCHING
    public bool AllowedToBranch(Vector3 worldDirection)
    {
        if (activeRootIndex >= 0 && playStarted && myRoots[activeRootIndex] != null && !viewingRoot)
        {
            return myRoots[activeRootIndex].AllowedToBranch(worldDirection);
        }
        else
        {
            return false;
        }
    }

    public void CreateBranchInWorldDirection(Vector3 direction)
    {
        if (AllowedToBranch(direction))
        {
            PhotonView.Get(this).RPC("RpcCreateBranchInWorldDirection", PhotonTargets.AllBuffered, activeRootIndex, myRoots[activeRootIndex].GetActiveTipPosition(), direction);
        }
    }
    [PunRPC]
    private void RpcCreateBranchInWorldDirection(int rootIndex, Vector3 branchStart, Vector3 direction)
    {
        myRoots[rootIndex].CreateBranchInWorldDirection(branchStart, direction);
    }


    // NEW ROOT
    private void CreateNewRoot(int rootIndex)
    {
        // force destroy old root at this position, even if it is still in its death animation
        if (myRoots[rootIndex] != null)
        {
            myRoots[rootIndex].ForceDestroy();
        }

        // create a new root
        Vector3 newPos, newFacing;
        MapData.Instance.GetRootStartForPlayer(playerIndex, rootIndex, out newPos, out newFacing);
        myRoots[rootIndex] = PhotonNetwork.Instantiate(rootPrefabName, newPos, Quaternion.LookRotation(Vector3.forward, newFacing), 0).GetComponent<Root>();

        PhotonView.Get(this).RPC("RpcCreateNewRoot", PhotonTargets.AllBuffered, rootIndex, PhotonView.Get(myRoots[rootIndex]).viewID);
    }
    [PunRPC]
    private void RpcCreateNewRoot(int rootIndex, int pvID)
    {
        myRoots[rootIndex] = PhotonView.Find(pvID).GetComponent<Root>();

        // set its root index and player ref
        myRoots[rootIndex].rootIndex = rootIndex;
        myRoots[rootIndex].player = this;

        if (onRootReadyToGrow != null)
        {
            onRootReadyToGrow.Invoke(rootIndex);
        }

        if(viewContext != null)
        {
            viewContext.RootReadyVisuals(myRoots[rootIndex]);
        }

        if (rootIndex == activeRootIndex && viewContext != null)
        {
            viewContext.ui.ResetAbilityChooser();
            viewContext.ui.SetShowAbilityChooser(true);
        }
    }

    // START GROWING ROOT
    public void StartGrowingActiveRoot()
    {
        StartGrowingRoot(activeRootIndex);
    }
    private void StartGrowingRoot(int rootIndex)
    {
        if (playStarted && activeRootIndex >= 0 && myRoots[activeRootIndex] != null && !myRoots[rootIndex].hasStarted)
        {
            PhotonView.Get(this).RPC("RpcStartGrowingRoot", PhotonTargets.AllBuffered, rootIndex, viewContext.ui.GetAbilityIndex());
        }
    }
    [PunRPC]
    private void RpcStartGrowingRoot(int rootIndex, int abilityIndex)
    {
        myRoots[rootIndex].SetAbility(abilityIndex);

        if (viewContext != null)
        {
            viewContext.ui.ResetAbilityChooser();
            viewContext.ui.SetShowAbilityChooser(false);
        }

        myRoots[rootIndex].StartGrowing();

        if (onRootStartsGrowing != null)
        {
            onRootStartsGrowing.Invoke(rootIndex);
        }
    }


    // CUTTING
    public void CutBranch(int rootIndex)
    {
        PhotonView.Get(this).RPC("RpcCutBranch", PhotonTargets.AllBuffered, rootIndex, myRoots[rootIndex].GetActiveTipPosition());
    }
    [PunRPC]
    private void RpcCutBranch(int rootIndex, Vector3 tipEnd)
    {
        myRoots[rootIndex].Cut(tipEnd);

        if (onRootDies != null)
        {
            onRootDies.Invoke(rootIndex);
        }
    }

    // COLLISION
    public void BranchCollided(int rootIndex, int tipIndex, Vector3 tipEnd, GameObject collidedObj)
    {
        if(collidedObj.GetComponentInParent<Pool>())
        {
            Debug.Log("hit a pool");
            PhotonView.Get(this).RPC("RpcBranchCollided", PhotonTargets.AllBuffered, rootIndex, tipIndex, myRoots[rootIndex].GetBranchTipEnd(tipIndex), collidedObj.GetComponentInParent<Pool>().id.ID, -1);
        }
        else if (collidedObj.GetComponent<Branch>())
        {
            Debug.Log("hit a branch");
            PhotonView.Get(this).RPC("RpcBranchCollided", PhotonTargets.AllBuffered, rootIndex, tipIndex, myRoots[rootIndex].GetBranchTipEnd(tipIndex), -1, collidedObj.GetComponent<Branch>().GUID);
        }
        else
        {
            Debug.Log("hit an obstacle");
            PhotonView.Get(this).RPC("RpcBranchCollided", PhotonTargets.AllBuffered, rootIndex, tipIndex, myRoots[rootIndex].GetBranchTipEnd(tipIndex), -1, -1);
        }
    }
    [PunRPC]
    private void RpcBranchCollided(int rootIndex, int tipIndex, Vector3 tipEnd, int poolID, int branchID)
    {
        if (poolID > 0)
        {
            myRoots[rootIndex].CollidedWithObject(tipIndex, tipEnd, Pool.Find(poolID).gameObject);
        }
        else if(branchID > 0)
        {
            myRoots[rootIndex].CollidedWithObject(tipIndex, tipEnd, Branch.Find(branchID).gameObject);
        }
        else
        {
            myRoots[rootIndex].CollidedWithObject(tipIndex, tipEnd, null);
        }
    }

    public void CutOtherBranch(int branchID, Vector3 tipEnd)
    {
        PhotonView.Get(this).RPC("RpcCutOtherBranch", PhotonTargets.AllBuffered, branchID, tipEnd);
    }
    [PunRPC]
    private void RpcCutOtherBranch(int branchID, Vector3 tipEnd)
    {
        Branch.Find(branchID).ChopAtPoint(tipEnd);
    }

    // FIRE
    public void CatchFire(int rootIndex, int tipIndex, float growthAtTime)
    {
        PhotonView.Get(this).RPC("RpcCatchFire", PhotonTargets.AllBuffered, rootIndex, tipIndex, growthAtTime);
    }
    [PunRPC]
    private void RpcCatchFire(int rootIndex, int tipIndex, float growthAtTime)
    {
        myRoots[rootIndex].TipCatchFire(tipIndex, growthAtTime);
    }

    #endregion

}
